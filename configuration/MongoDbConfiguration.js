module.exports = {
    url: "mongodb://COMPARTAMOS-CORE:27001/EVEREST_USSD",
    loggerLevel: 'DEBUG',
    loggerName: 'MONGO ENGINE',
    user: 'everest_ussd',
    pass: 'everestdbpassword',
    authdb: 'admin',
    poolSize: 100,
    certLoc : '/opt/node/ESB/config/ssl/mongodb/MEX/Compartemos/mongodb-live-db1-cert.crt'
};
