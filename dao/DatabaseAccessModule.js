"use strict";

/*
 Database access module
 */

var log4js = require('log4js'),
    MongoConfig = require('./../configuration/MongoDbConfiguration');

var DatabaseAccessConfig = {
    loggerName: 'DATABASE ACCESS',
    loggerLevel: MongoConfig.loggerLevel
};

var logger = log4js.getLogger(DatabaseAccessConfig.loggerName);
logger.setLevel(DatabaseAccessConfig.loggerLevel);

function processOperation(databaseParameters, callback) {
    var functionType = databaseParameters.functionType;
    var modelName = databaseParameters.modelName;
    var Model = require('../models/' + modelName + 'Model');
    if (functionType == 'populate') {
        processPopulate(databaseParameters, callback);
    } else if (functionType == 'custom') {
        processCustomMethod(databaseParameters, Model, callback)
    } else {
        switch (functionType) {
            case 'find':
                findRecords(databaseParameters, Model, callback);
                break;
            case 'findOne':
                findRecord(databaseParameters, Model, callback);
                break;
            case 'save':
                saveRecord(databaseParameters, Model, callback);
                break;
            case 'update':
                updateRecord(databaseParameters, Model, callback);
                break;
            case 'findAndModify':
                findAndModifyRecord(databaseParameters, Model, callback);
                break;
            case 'delete':
                deleteRecord(databaseParameters, Model, callback);
                break;
            default:
                databaseParameters.responseCode = 'DA-99';
                databaseParameters.responseString = 'Unsupported method.';
                callback(databaseParameters);
                break;
        }
    }
}

function findRecord(databaseParameters, Model, callback) {
    logger.debug('findRecord()', databaseParameters);
    Model.findOne(databaseParameters.findConditions, function (error, result) {
        if (error) {
            logger.error(error.stack);
            databaseParameters.responseCode = 'DA-02';
            databaseParameters.responseString = error.message;
        } else if (!result) {
            databaseParameters.responseCode = 'DA-03';
            databaseParameters.responseString = 'No record found.';
        } else {
            databaseParameters.responseCode = 'DA-00';
            databaseParameters.responseString = 'Record found.';
            databaseParameters.result = result;
        }
        callback(databaseParameters);
    });
}

function findRecords(databaseParameters, Model, callback) {
    logger.debug('findRecords()' + JSON.stringify(databaseParameters));
    var findConditions = databaseParameters.findConditions;
    Model.find(findConditions, function (error, result) {
        if (error) {
            logger.error(error.stack);
            databaseParameters.error = error;
            databaseParameters.responseCode = 'DA-FR-02';
            databaseParameters.responseString = error.message;
        } else if (!result) {
            databaseParameters.responseCode = 'DA-FR-03';
            databaseParameters.responseString = 'No record found.';
        } else {
            databaseParameters.responseCode = 'DA-00';
            databaseParameters.responseString = 'Record found.';
            databaseParameters.result = result;
        }
        callback(databaseParameters);
    });
}

function saveRecord(databaseParameters, Model, callback) {

    logger.debug('saveRecord()', databaseParameters);
    var modelInstance = new Model(databaseParameters.instanceInfo);

    modelInstance.save(function (error, record) {
        if (error) {
            databaseParameters.error = error;
            logger.error(error);
            databaseParameters.responseCode = 'DA-SV-01';
            databaseParameters.responseString = error.message;
        } else {
            databaseParameters.responseCode = 'DA-00';
            databaseParameters.responseString = 'Record saved.';
            databaseParameters.record = record;
        }
        callback(databaseParameters);
    })
}

function updateRecord(databaseParameters, Model, callback) {

    Model.update(databaseParameters.findConditions, databaseParameters.update, function (error, result) {
        if (error) {
            logger.error(error.stack);
            databaseParameters.responseCode = 'DA-UP-03';
            databaseParameters.responseString = error.message;
            databaseParameters.error = error;
        } else if (!result) {
            databaseParameters.responseCode = 'DA-UP-04';
            databaseParameters.responseString = 'No record found to update.';
        } else {
            databaseParameters.responseCode = 'DA-00';
            databaseParameters.responseString = 'Record updated.';
            databaseParameters.result = result;
        }
        callback(databaseParameters);
    });

}

function deleteRecord(databaseParameters, Model, callback) {

}

function processPopulate(databaseParameters, callback) {

}

function processCustomMethod(databaseParameters, Model, callback) {
    var functionName = databaseParameters.functionName;
    Model[functionName](databaseParameters, callback);

}

module.exports = {
    processOperation: processOperation
};

