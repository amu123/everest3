"use strict";
/*
 SharedServices startup module
 */

var EventEmitter = require('events').EventEmitter,
    util = require('util');

function EVEREST_USSD() {

    //var fs = require('graceful-fs'),
    var fs = require('fs'),
        async = require('async');

    var EVEREST_USSD_Server = this;

    var Configuration = {
        loggerLevel: 'DEBUG',
        loggerName: 'EVEREST_USSD',
        components: [
            {
                path: './engines/',
                type: 'ENGINES'
            }
        ]
    };

    var log4js = require('log4js');
    var logger = log4js.getLogger(Configuration.loggerName);

    log4js.configure({
        appenders: [
            {
                type: 'console'
            },
            {
                type: 'dateFile',
                absolute: false,
                filename: './log/EVEREST_USSD.log',
                pattern: '-yyyy-MM-dd-hh',
                alwaysIncludePattern: false
            }
        ], replaceConsole: true
    });
    logger.setLevel(Configuration.loggerLevel);

    function loadComponents(componentsPath, typeofComponent, outerCallback) {
        var directoryContents = fs.readdirSync(componentsPath);
        if (directoryContents.length == 0) {
            logger.warn('NO ' + typeofComponent + ' CURRENTLY DEPLOYED!');
            outerCallback();
            return;
        }
        async.eachSeries(directoryContents,
            function (componentPath, innerCallback) {
                var absolutePath = componentsPath + componentPath;
                if (absolutePath.indexOf('.js') > 0) {
                    var component = require(absolutePath).getComponent();

                    component.on('ready', function () {
                        logger.debug(absolutePath);
                        innerCallback();
                    });

                    component.on('error', function (error) {
                        innerCallback(error);
                    });

                    component.emit('start');
                } else {
                    innerCallback();
                }
            },
            function (error) {
                if (error) {
                    outerCallback(error);
                } else {
                    logger.info(typeofComponent + ' READY');
                    outerCallback();
                }
            })
    }

    function start() {
        async.eachSeries(Configuration.components, function (component, callback) {
            EVEREST_USSD_Server.emit('loadComponents', component.path, component.type, callback)
        }, function (error) {
            if (error) {
                logger.fatal(error.stack);
            } else {
                logger.info('SERVICES READY');
            }
        })
    }

    EVEREST_USSD_Server.on('loadComponents', loadComponents);
    EVEREST_USSD_Server.once('start', start);

    EventEmitter.call(EVEREST_USSD_Server);
}

util.inherits(EVEREST_USSD, EventEmitter);

new EVEREST_USSD().emit('start');

