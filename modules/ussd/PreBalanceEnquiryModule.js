var BalanceEnquiryModule = require('./BalanceEnquiryModule'),
    modConfig            = require('./moduleHandler/ModuleConfig');
    log4js               = require('log4js');

var logger  = log4js.getLogger('PRE-BALANCE:');

exports.processRequest = function (sessionInformation, callback) {
    sessionInformation.status = 'SUCCESS';
    var accounts = JSON.parse(sessionInformation.accounts);
    var message  = modConfig.module.airtime.message;
    if (accounts.length == 1) {
        sessionInformation.accountNumber = accounts[0].accountNumber;
        sessionInformation.trans_type = accounts[0].product.tran_types[1].code;
        BalanceEnquiryModule.processRequest(sessionInformation, function (sessionInformation) {
            sessionInformation.response = [sessionInformation.availableBalance, sessionInformation.currentBalance, sessionInformation.accountName];
            callback(sessionInformation);
        })
    }else {
        sessionInformation.responseCode = 'PBEM-00';
        sessionInformation.responseString = 'Select account';
        sessionInformation.response = [message + sessionInformation.accountDisplay];
        callback(sessionInformation);
    }
};