var log4js          = require('log4js'),
    logger          = log4js.getLogger('MINI STATEMENT'),
    esbAccess       = require('./connector/EsbAccessModule'),
    modConfig       = require('./moduleHandler/ModuleConfig'),
    refEdit         = require('./RefAssignModule');

exports.processRequest = function (sessionInfo, callback) {
    var message = '';
    sessionInfo.fromAcc = sessionInfo.accountNumber;
    esbAccess.transaction(sessionInfo, function(data){
        var _code   = data.data.code;
        var accType = data.data.accountType;
        switch(_code){
            case '00':
                if(accType === 'ACCT'){
                    if(data.data.statement_result){
                        var statements = data.data.statement_result;
                        refEdit.processRequest(statements, sessionInfo, function(shortRef){
                            sessionInfo.miniStatement    = shortRef;
                            sessionInfo.responseCode = 'MST-00';
                            callback(sessionInfo);
                        });
                    }else{
                        message = modConfig.module.statement.noTransMessage + '\n' + modConfig.module.bckMessage;
                        sessionInfo.responseCode = 'MST-01';
                        sessionInfo.miniStatement = message;
                        callback(sessionInfo);
                    }
                }else{
                    message = modConfig.module.statement.invalidAccType + '\n' + modConfig.module.bckMessage;
                    sessionInfo.responseCode = 'MST-02';
                    sessionInfo.miniStatement = message;
                    callback(sessionInfo);
                }
                break;
            default:
                message = modConfig.module.techError;
                sessionInfo.responseCode = 'TMST-03';
                callback(sessionInfo);
        }
    });
};