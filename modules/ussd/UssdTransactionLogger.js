/**
 * Created by ntokozo on 2016/07/26.
 */
var log4js          = require('log4js'),
    dbAccess        = require('./connector/DatabaseAccessModule');

var logger = log4js.getLogger('TRANS LOGGER: ');

exports.logTrans=function(sessionInfo, esbData, callback){
    logger.info('ESB DATA: ', esbData);
    sessionInfo.modelName='UssdTransaction';
    sessionInfo.queryType='save';

    dbAccess.processRequest(sessionInfo, function (instanceInfo) {
        callback(sessionInfo);
    });
};