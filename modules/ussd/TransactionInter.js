var modConfig   = require('./moduleHandler/ModuleConfig');
var acc         = require('./PreTransactionModule');
exports.processRequest=function(sessionInfo, callback){
    sessionInfo.fromAcc = sessionInfo.accountNumber;
    var message = modConfig.module.transactionInter.toMessage;
    sessionInfo.responseCode    = 'INTA-00';
    sessionInfo.display         = [message];
    callback(sessionInfo);
};


