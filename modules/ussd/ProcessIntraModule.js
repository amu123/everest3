var esbAccess = require('./connector/EsbAccessModule');
var modConfig = require('./moduleHandler/ModuleConfig');

exports.processRequest  = function(sessionInfo, callback){
    var option = sessionInfo.input;

    switch (option){
        case '0':
            esbAccess.transaction(sessionInfo, function (esbResponse) {
                var back    = modConfig.module.bckMessage;
                var message ='';
                if(esbResponse){
                    var code = esbResponse.code;
                    switch (code){
                        case '00':
                            sessionInfo.responseCode = 'PIMOD-00';
                            message = esbResponse.message + '\n' + back;
                            sessionInfo.response     = [message];
                            callback(sessionInfo);
                            break;
                        case '06':
                            sessionInfo.responseCode ='PIMOD-01';
                            message = esbResponse.message + '\n' + back;
                            sessionInfo.response     = [message];
                            callback(sessionInfo);
                            break;
                    }
                }else{
                    sessionInfo.responseCode ='UBM-01';
                    sessionInfo.response     = ['Technical Problem Occured' + back];
                }
            });
            break;
        case '9':
            sessionInfo.status = 'SUCCESS';
            sessionInfo.responseCode = 'PIMOD-03';//'TCPM-03';
            callback(sessionInfo);
            break;
    }
};