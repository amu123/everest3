exports.processRequest = function (sessionInformation, callback) {
    var accounts = JSON.parse(sessionInformation.accounts);

    if (accounts.length == 1) {
        sessionInformation.accountNumber = accounts[0].accountNumber;
        sessionInformation.responseCode = 'APM-01';
        sessionInformation.responseString = 'Defaulted to only account number.';
        callback(sessionInformation);
    } else {
        sessionInformation.response = [sessionInformation.accountDisplay];
        sessionInformation.responseCode = 'APM-00';
        sessionInformation.responseString = 'Select account.';
        callback(sessionInformation);
    }
};
