var modConfig       = require('./moduleHandler/ModuleConfig');
var log4js          = require('log4js');
var logger          = log4js.getLogger();
exports.processRequest = function(sessionInfo, callback){
    logger.info('**************************TRANSACTION LOAN!!', sessionInfo.accountNumber);
    sessionInfo.fromAcc = sessionInfo.accountNumber;
    var message = modConfig.module.transactionLoan.toMessage;
    sessionInfo.findModule      = modConfig.module.transactionLoan.name;
    sessionInfo.modCode         = modConfig.module.transactionLoan.code;
    sessionInfo.responseCode = 'TSHM-01';
    sessionInfo.display         = [message + sessionInfo.accountDisplay];
    callback(sessionInfo);
};