var modConfig   = require('./moduleHandler/ModuleConfig');
var acc         = require('./PreTransactionModule');
var log4js      = require('log4js');
var logger      = log4js.getLogger('TRANSACTION MODULE');
exports.processRequest=function(sessionInfo, callback){
    sessionInfo.fromAcc = sessionInfo.accountNumber;
    var message = modConfig.module.transaction.toMessage;
    logger.info('______________________PROMPT_TO_ACC______________________');
    sessionInfo.findModule      = modConfig.module.transaction.transModule.name;
    sessionInfo.modCode         = modConfig.module.transaction.transModule.code;
    sessionInfo.responseCode    = 'TSHM-00';
    sessionInfo.display         = [message + sessionInfo.accountDisplay];
    callback(sessionInfo);
};
