var modConfig   = require('./moduleHandler/ModuleConfig');

exports.processRequest = function(sessionInformation, callback){
    var msgOptions = modConfig.module.transaction.options;
    var display =' 1. ' +msgOptions.intra +'\n' + '2.' + msgOptions.inter + '\n' + ' 3. ' + msgOptions.mobile + ' \n' + ' 4. ' + msgOptions.loan + ' \n ' + ' 9. ' + msgOptions.back;
    sessionInformation.responseCode = 'TOM-00';
    sessionInformation.responseString = 'Select account';
    sessionInformation.response = [display];
    callback(sessionInformation);
};