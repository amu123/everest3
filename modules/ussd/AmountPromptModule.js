var log4js = require('log4js'),
    logger = log4js.getLogger('AMOUNT PROMPT: '),
    esbAccess   = require('./connector/EsbAccessModule'),
    modConfig   = require('./moduleHandler/ModuleConfig');


exports.processRequest=function(sessionInfo, callback){
    var amountArr             = JSON.parse(sessionInfo.AmntData);
    var message               ='';
    sessionInfo.amount        = amountArr[1].amount;
    sessionInfo.product_id    = amountArr[1].sku;
    esbAccess.transaction(sessionInfo, function(data){
        var _code = data.code;
        if(data){
            switch(_code){
                case '00':
                    message = data.message + '\n' + modConfig.module.bckMessage;
                    sessionInfo.responseCode = "AiPM-00";
                    sessionInfo.response = [message];
                    callback(sessionInfo);
                    break;
                case '49':
                    message = data.message + '\n' + modConfig.module.bckMessage;
                    sessionInfo.responseCode = "AiPM-01";
                    sessionInfo.response = [message];
                    callback(sessionInfo);
                    break;
            }
        }else{
            sessionInfo.responseCode = "AiPM-01";
            sessionInfo.response = ['A Technical Error Occured.'];
            callback(sessionInfo);
        }
    });
};
