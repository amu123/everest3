/**
 * Created by ntokozo on 2016/07/08.
 */
var UssdMenuManagerConfig = {
    loggerName: 'USSD Menu Manager',
    loggerLevel: 'INFO',
    menuPath: '../config/Menu/',
    propertyPath: '../config/Properties/UssdProperties.json',
    encoding: 'utf8'
};

module.exports = UssdMenuManagerConfig;