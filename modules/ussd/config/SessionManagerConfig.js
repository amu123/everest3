/**
 * Created by ntokozo on 2016/07/08.
 */
var SessionManagerConfig = {
    redisHostIP : 'localhost',
    redisPort : 6379,
    redisOptions : {},
    loggerLevel : 'INFO'
}

module.exports = SessionManagerConfig;

'/home/ntokozo/Development/everest3-ussd/modules/ussd/config/SessionManagerConfig.js'