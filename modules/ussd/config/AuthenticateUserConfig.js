/**
 * Configuration module for USSD AuthenticateUserModule
 */
var AuthenticateCustomerConfig = {
    loggerName : 'AUTHENTICATE USER',
    loggerLevel: 'INFO'
}

module.exports = AuthenticateCustomerConfig