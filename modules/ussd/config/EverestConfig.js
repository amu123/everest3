/**
 * Created by ntokozo on 2016/07/08.
 */
var EverestVariables = {
    maxPinTries: 3,
    maxPasswordTries: 3,
    canResumeSession: false,
    expireSession: null,
    pinTryThreshold: 2,
    maxPageSize: 159,
    maxMsisdnLength: 11,
    inputMsisdnLength: 10
}

/*
 Formats msisdn (cellphone number) into international format
 */
exports.formatMsisdn = function (msisdn) {
    var returnString = msisdn;
    if (msisdn.length < EverestVariables.maxMsisdnLength) {
        returnString = '27' + msisdn.substring(1);
    }
    return returnString;

};

/*
 Verifies msisdn (cellphone number)
 */
exports.verifyMsisdn = function (msisdn) {
    return (msisdn != undefined && msisdn != null && msisdn.length == EverestVariables.inputMsisdnLength && msisdn.slice(0, 1) == '0');
};

exports.EverestVariables = EverestVariables;