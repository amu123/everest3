/**
 * Created by ntokozo on 2016/07/13.
 */
var mongoose    = require('mongoose'),
    Schema      = mongoose.Schema;

var UserChannelOrganizationAccessSchema = new Schema({
    identifier: String,
    channel: String,
    organizationID: String,
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    loginInfo : {
        encryptedPin: {type: String, default: ''},
        pinHistory: {type:Array, length: 5},
        pinTries: {type: Number, default: 0},
        pinExpiryDate: Date,
        reIssuePin: Boolean,
        encryptedPassword: {type: String, default: ''},
        passwordHistory: {type: Array, length: 5},
        passwordTries: {type: Number, default: 0},
        passwordExpiryDate: Date,
        OTP: String,
        OTPExpiryDate: Date,
        IsOnLoginPage: Boolean,
        tempPin: String,
        shouldReIssuePin: {type: Boolean, default: false}
    },
    dateCreated: {type: Date, default: Date.now()},
    lastDateModified: Date,
    createdBy: String,
    lastModifiedBy: String,
    isActive: Boolean,
    onUsLimit: {type: Number, default: 5000},
    notOnUsLimit: {type: Number, default: 5000},
    onUsSpent: {type: Number, default: 0},
    notOnUsSpent: {type: Number, default: 0},
    lastTransactionDate: {type: Date, default: new Date()},
    accountAccess: [{type: mongoose.Schema.Types.ObjectId, ref: 'Account'}]
});

//UserChannelOrganizationAccessSchema.index({identifier: 1, channel: 1, organizationID: 1}, {unique: true});

module.exports=mongoose.model('UserChannelOrganizationAccess', UserChannelOrganizationAccessSchema);