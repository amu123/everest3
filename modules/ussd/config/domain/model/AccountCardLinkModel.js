/**
 * Created by ntokozo on 2016/07/14.
 */
var mongoose        = require('mongoose'),
    Schema          = mongoose.Schema;

var AccountCardLinkSchema = new Schema({
    linkNumber: String,
    organizationID: String,
    linkNumberType: String,
    cardNumber: String,
    isIssued: {type: Boolean, default: false},
    creationDate: {type: Date, default: Date.now()},
    issuedDate: {type: Date},
    createdBy: String,
    account: {type: Schema.Types.ObjectId, ref: 'Account'},
    isActive: {type: Boolean, default: false},
    activatingAgentID: String
});

//AccountCardLinkSchema.index({linkNumber: 1, organizationID: 1, linkNumberType: 1}, {unique: true});

module.exports=mongoose.model('accountcardlinks', AccountCardLinkSchema);