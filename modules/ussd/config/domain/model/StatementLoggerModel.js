/**
 * Created by ntokozo on 2016/07/27.
 */
var mongoose        = require('mongoose'),
    Schema          = mongoose.Schema;

var statementSchema = new Schema({
    statement       : [{TrReference     :{type: String, required:false},
        TrType          :{type: String, required:false},
        TrStatus        :{type: String, required:false},
        TrDescription   :{type: String, required:false},
        TrDate          :{type: Date,   required:false},
        TrAmount        :{type: String, required:false},
        Currency        :{type: String, required:false},
        trAssignedRef   :{type: String, required:false},
        shortRef        :{type: String, required:false}}]
});

module.exports=mongoose.model('statement', statementSchema);