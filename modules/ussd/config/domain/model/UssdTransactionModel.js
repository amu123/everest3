var mongoose        = require('mongoose'),
    Schema          = mongoose.Schema;

var UssdTransactionSchema = new Schema({
    transactionLogDate      : {type:Date, required:false},
    userMsisdn              : {type: String, required:false},
    userRequest             : {type: String, required:false},
    systemResponse          : {type: String, required:false},
    organizationID          : {type: String, required:false},
    fullRef                 : {type: String, required:false},
    shortRef                : {type: String, required:false}
});

module.exports=mongoose.model('ussdTrans', UssdTransactionSchema);