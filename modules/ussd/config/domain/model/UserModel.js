/**
 * Created by ntokozo on 2016/07/14.
 */
var mongoose    = require('mongoose'),
    Schema      = mongoose.Schema;

var UserSchema = new Schema({
    legacyUserID: String,
    name: String,
    surname: String,
    dateCreated: {type: Date, default: Date.now()},
    dateModified: Date,
    createdBy: String,
    lastModifiedBy: String,
    isActive: Boolean,
    isAgent: [String],
    language: String,
    accounts: [{type: mongoose.Schema.Types.ObjectId, ref: 'Account'}],
    userChannelOrganizationAccess: [{type: mongoose.Schema.Types.ObjectId, ref: 'UserChannelOrganizationAccess'}],
    identification: {
        identificationNumber: String,
        identificationType: Number
    },
    agentID: String
});

// UserSchema.index({identification: 1}, {unique: false});
// UserSchema.index({legacyUserID: 1}, {unique: true});

module.exports=mongoose.model('user', UserSchema);