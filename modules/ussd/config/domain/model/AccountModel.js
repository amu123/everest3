/**
 * Created by ntokozo on 2016/07/14.
 */
var mongoose    = require('mongoose'),
    Schema      = mongoose.Schema;

var AccountSchema = new Schema({
    organizationID: String,
    accountNumber: String,
    dateCreated: {type: Date, default: Date.now()},
    dateModified: Date,
    createdBy: String,
    lastModifiedBy: String,
    accountName: String,
    isActive: Boolean,
    approvalsRequiredPayments: Number,
    approvalsRequiredCollections: Number,
    allowCollections: Boolean,
    _default: Boolean,
    owner: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    users: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    internetBankingTransactions:[{ type: mongoose.Schema.Types.ObjectId, ref: 'InternetBankingTransactionSchema' }],
    accountCardLink: {type: mongoose.Schema.Types.ObjectId, ref: 'AccountCardLink'},
    legacyModifiedDate: Date
});

//AccountSchema.index({accountNumber: 1, organizationID: 1}, {unique: true});

module.exports = mongoose.model('Account', AccountSchema);