/**
 * Created by ntokozo on 2016/07/13.
 */
var log4js = require('log4js'),
    DatabaseAccess = require('./connector/DatabaseAccessModule');

exports.processRequest = function (sessionInformation, callback) {
    var logger = log4js.getLogger('VERIFY TRACER - ' + sessionInformation.agentMsisdn);

    sessionInformation.input="101234567";

    var tracerNumber = sessionInformation.input;
    var regex = new RegExp(sessionInformation.regex);

    //var findValue=pattern.exec();
    logger.info('REGEX NUMBER! ', sessionInformation.regex);

    if (!tracerNumber.match(regex)) {
        sessionInformation.responseCode = 'VTNM-04';
        sessionInformation.responseString = 'Invalid Tracer Number';
        return callback(sessionInformation);
    }

    var databaseParameters = {
        conditions: {
            linkNumber: sessionInformation.input,
            organizationID: '627696',
            linkNumberType: 'starter'
        },
        queryType: 'findOne',
        modelName: 'AccountCardLink'
    };

    DatabaseAccess.processRequest(databaseParameters, function (result) {
        var accountCardLink = result.information;
        if (result.error) {
            sessionInformation.responseCode = 'VTNM-03';
            sessionInformation.responseString = 'DATABASE ACCESS ERROR!';
            return callback(sessionInformation);
        } else if (!accountCardLink) {
            sessionInformation.responseCode = 'VTNM-02';
            sessionInformation.responseString = 'Invalid tracer number.';
            return callback(sessionInformation);
        } else if (accountCardLink.isIssued) {
            sessionInformation.responseCode = 'VTNM-01';
            sessionInformation.responseString = 'Tracer number already issued.';
            return callback(sessionInformation);
        } else {
            sessionInformation.responseCode = 'VTNM-00';
            sessionInformation.responseString = 'Tracer number valid.';
            sessionInformation.accountCardLink = JSON.stringify(accountCardLink);
            sessionInformation.tracerNumber = tracerNumber;
            return callback(sessionInformation);
        }
    })

};