/**
 * Created by ntokozo on 2016/07/11.
 */
var log4js = require('log4js'),
    request     = require('request'),
    DatabaseAccess = require('./connector/DatabaseAccessModule');

var logger = log4js.getLogger('VERIFY AGENT');

exports.processRequest = function (sessionInformation, callback) {
    sessionInformation.status = 'FAILURE';
    var msisdn = sessionInformation.msisdn;
    var organizationID = sessionInformation.organizationID;

    sessionInformation.conditions = {
        identifier: msisdn,
        organizationID: "COMPARTAMOS",
        channel: 'USSD'
    };

    sessionInformation.modelName = 'UserChannelOrganizationAccess';
    sessionInformation.queryType = 'findOne';
    
    try{
        request({
            url:'http://10.5.2.1:9091/ESBConnector/createSession',
            method:'GET',
            json:{
                channel         : "USSD",
                organizationID  : "COMPARTAMOS",
                deviceId        : "",
                application     : "COMPARTAMOS USSD"
            }
        }, function(err, response, body){
            if(err){
                callCallback('VUM-01', 'Error has occurred accessing User records.');
            }else if(!body){
                callCallback('VUM-02', 'Record information missing in users!');
            }else{
                logger.info('BODY: ', body);
                sessionInformation.username = 'Takonewa';
                sessionInformation.agentFullName = sessionInformation.username + ' ' + 'Moyo';
                sessionInformation.agentMsisdn = sessionInformation.msisdn;
                sessionInformation.status = 'SUCCESS';
                sessionInformation.encryptedPin='1234';
                callCallback('VUM-00', 'User verified.');
                //callCallback(body.code, body.message);
            }
        })
    }catch(e){
        logger.fatal(e.stack);
    }

    function callCallback(responseCode, responseString) {
        if (sessionInformation.error) {
            logger.error(sessionInformation.error.stack);
            sessionInformation.error = null;
        }
        sessionInformation.responseCode = responseCode;
        sessionInformation.responseString = responseString;
        callback(sessionInformation);
    }
};