var airtimePurchase     = require('./AirtimePurchaseModule');
var modConfig           = require('./moduleHandler/ModuleConfig');

exports.processRequest = function (sessionInformation, callback) {
    switch(sessionInformation.input){
        case '1':
            if(sessionInformation.toMsisdn){
                sessionInformation.topupmsisdn = sessionInformation.toMsisdn;
            }else {
                sessionInformation.topupmsisdn = sessionInformation.msisdn;
            }
            sessionInformation.status = 'SUCCESS';
            var accounts = JSON.parse(sessionInformation.accounts);
            var message  = modConfig.module.airtime.message;
            if (accounts.length == 1) {
                sessionInformation.accountNumber = accounts[0].accountNumber;
                sessionInformation.trans_type = accounts[0].product.tran_types[0].code;
                sessionInformation.Currency   = accounts[0].currency.code;
                airtimePurchase.processRequest(sessionInformation, function (airtimeData) {
                    sessionInformation.response = [airtimeData.s_provider];
                    callback(sessionInformation);
                })
            }else {
                sessionInformation.findModule = modConfig.module.airtime.name;
                sessionInformation.modCode = modConfig.module.airtime.code;
                sessionInformation.trans_type=accounts[0].product.tran_types[0].code;
                sessionInformation.responseCode = 'PAIR-00';
                sessionInformation.responseString = 'Select account';
                sessionInformation.response = [message + sessionInformation.accountDisplay];
                callback(sessionInformation);
            }
            break;
        case '2':
            var promptMsg = modConfig.module.airtime.msisdnPrompt;
            sessionInformation.responseCode = 'MSPR-00';
            sessionInformation.responseString = 'Enter MSISDN';
            sessionInformation.response = [promptMsg];
            callback(sessionInformation);
            break;
        case '3':
            var invalidMsisdn = modConfig.module.airtime.invalidMsisdn;
            sessionInformation.responseCode = 'MSPRI-00';
            sessionInformation.response = [invalidMsisdn];
            callback(sessionInformation);
            break;
        case '9':
            var msg = modConfig.module.bckMessage;
            sessionInformation.status = 'SUCCESS';
            sessionInformation.responseCode = 'MSPRI-01';
            sessionInformation.response = [msg];
            callback(sessionInformation);
    }
};