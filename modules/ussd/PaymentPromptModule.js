var modConfig = require('./moduleHandler/ModuleConfig');
exports.processRequest = function (sessionInformation, callback) {
    sessionInformation.fromAcc = sessionInformation.accountNumber;
    var message = modConfig.module.amntPrompt;
    var input = sessionInformation.input;
    var skuArr = sessionInformation.serviceBillSku.split(',');
    var IDArr  = sessionInformation.serviceBillID.split(',');
    sessionInformation.sku = skuArr[input - 1];
    sessionInformation.product_id = IDArr[input - 1];
    sessionInformation.responseCode = 'PPMP-00';
    sessionInformation.response = [message];
    callback(sessionInformation);
};