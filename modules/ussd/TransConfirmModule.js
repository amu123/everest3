var modConfig = require('./moduleHandler/ModuleConfig');
var esbAccess = require('./connector/EsbAccessModule');
var transT = require('./ProcessTransType');

exports.processRequest  = function(sessionInfo, callback){
    var message = '';
    var confirmation = modConfig.module.transactionInter.confirm;
    var option = '';

    transT.getType(sessionInfo, function(nextStep){
        if(nextStep === 'local'){
            var centAmnt = sessionInfo.input;
            sessionInfo.amount = sessionInfo.fullAmnt + '.' + sessionInfo.input;
            message = modConfig.module.transactionInter.confirmIntraMessage;
            var mapObj = {strAmnt:sessionInfo.amount, strAcc:sessionInfo.toAcc};
            message = message.replace(/strAmnt|strAcc/gi, function (matched) {
                return mapObj[matched];
            });
            sessionInfo.responseCode = 'TCPM-11';
            sessionInfo.response = [message];
            callback(sessionInfo);
        }else if(nextStep === 'remote'){
            option = sessionInfo.input;
            switch (option){
                case '0':
                    sessionInfo.confirm = confirmation;
                    var transType       = sessionInfo.trans_type;
                    esbAccess.transaction(sessionInfo, function(esbConfirmation){
                        if(esbConfirmation){
                            var _code = esbConfirmation.code;
                            switch (_code){
                                case '00':
                                    message = 'Transaction successful ' + modConfig.module.bckMessage;
                                    sessionInfo.responseCode = 'TCPM-00';
                                    sessionInfo.response = [message];
                                    callback(sessionInfo);
                                    break;
                                default:
                                    sessionInfo.responseCode = 'TCPM-07';
                                    callback(sessionInfo);
                                    break;
                            }
                        }
                    });
                    break;
                case '9':
                    sessionInfo.status = 'SUCCESS';
                    sessionInfo.responseCode = 'TCPM-03';
                    callback(sessionInfo);
                    break;
                default:
                    sessionInfo.responseCode = 'TCPM-07';
                    callback(sessionInfo);
                    break;
            }
        }
    });
};