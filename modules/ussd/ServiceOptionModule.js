/**
 * Created by ntokozo on 2016/09/08.
 */
var modConfig = require('./moduleHandler/ModuleConfig');
var procBill = require('./ProcessBillModule');
var log4js = require('log4js');
var logger = log4js.getLogger('SERVICE OPTION MODULE');

exports.processRequest = function (sessionInformation, callback) {
    var accounts = JSON.parse(sessionInformation.accounts);
    var bckMsg   = modConfig.module.bckMessage;
    var message  = '';
    if(accounts.length === 1){
        message = modConfig.module.billPayment.billMsg;
        sessionInformation.accountNumber = accounts[0].accountNumber;
        sessionInformation.trans_type = accounts[0].product.tran_types[2].code;
        procBill.getBill(sessionInformation, function (cbData) {
            logger.info(JSON.stringify(cbData.display));
            var products = cbData.display;
            sessionInformation.responseCode = 'SOM-00';
            sessionInformation.response = [message + ' ' + products +  ' ' + bckMsg];
            callback(sessionInformation);
        });
    }else {
        message = modConfig.module.billPayment.message;
        sessionInformation.findModule = 'BillPayment';
        sessionInformation.modCode = '8';
        sessionInformation.responseCode = 'ASOM-00';
        sessionInformation.response = [message];
        callback(sessionInformation);
    }
};


