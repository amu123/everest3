var statement = require('./MiniStatementModule'),
    modConfig = require('./moduleHandler/ModuleConfig');
    log4js               = require('log4js');

var logger  = log4js.getLogger('PRE-MINI-STATEMENT:');

exports.processRequest = function (sessionInformation, callback){
    sessionInformation.status = 'SUCCESS';
    var accounts = JSON.parse(sessionInformation.accounts);
    var message  = modConfig.module.statement.message;
    if(accounts.length==1){
        sessionInformation.trans_type=accounts[0].product.tran_types[3].code;
        sessionInformation.accountNumber = accounts[0].accountNumber;
        statement.processRequest(sessionInformation, function (sessionInformation) {
        sessionInformation.response = [sessionInformation.miniStatement];
        callback(sessionInformation);
        })
    }else{
        sessionInformation.findModule = modConfig.module.statement.name;
        sessionInformation.modCode = modConfig.module.statement.code;
        sessionInformation.trans_type=accounts[0].product.tran_types[3].code;
        sessionInformation.responseCode = 'PMIN-00';
        sessionInformation.responseString = 'Select account';
        sessionInformation.response = [message + sessionInformation.accountDisplay];
        callback(sessionInformation);
    }
};