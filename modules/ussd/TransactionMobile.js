var modConfig       = require('./moduleHandler/ModuleConfig');
exports.processRequest = function(sessionInfo, callback){
    sessionInfo.fromAcc = sessionInfo.accountNumber;
    var message = modConfig.module.transactionMobile.toMessage;
    sessionInfo.responseCode = 'MTMS-00';
    sessionInfo.display      = [message];
    callback(sessionInfo);
};