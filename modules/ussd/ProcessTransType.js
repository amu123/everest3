var processType = function(){};

processType.prototype.getType = function(sessionInfo, callback){
    var type = sessionInfo.trans_type;
    var response = '';
    if (type === 'INTRA0001'){
        response = 'local';
        callback(response);
    }else if (type === 'INTTFR0001'){
        response = 'remote';
        callback(response);
    }
};
processType.prototype.getCode = function(option, callback){
    var _code = '';
    if(option === 'INTTFR0001'){
        _code = 'PRCE-00';
        callback(_code);
    }else if(option === 'INTRA0001'){
        _code = 'PRCE-01';
        callback(_code);
    }else if(option === 'SRV0001'){
        _code = 'PRCE-02';
        callback(_code);
    }else if(option === 'INTLN0001'){
        _code = 'LTRAN-00';
        callback(_code);
    }
};

module.exports = new processType();
