/**
 * Created by ntokozo on 2016/07/15.
 */
var  EverestConfig = require('./config/EverestConfig'),
    log4js = require('log4js');

exports.processRequest = function (sessionInfo, callback) {

    var logger = log4js.getLogger('AUTHENTICATE CUSTOMER: ' + sessionInfo.msisdn);
    logger.setLevel('INFO');
    
    var pinTries = parseInt(sessionInfo.pinTries);
    if (sessionInfo.input == sessionInfo.encryptedPin) {
        sessionInfo.status = 'SUCCESS';
        sessionInfo.pinTries = 0;
        updateUser('ACM-00', 'Input PIN valid.');
        logger.debug('PIN VALID');
        if (sessionInfo.shortCutSteps) {
            sessionInfo.shouldProcessShortCut = 'YES';
        }
    } else {
        sessionInfo.pinTries = ++pinTries;
        sessionInfo.status = 'FAILURE';
        if (sessionInfo.pinTries > EverestConfig.EverestVariables.pinTryThreshold) {
            updateUser('ACM-01', 'PIN tries exceeded.');
            logger.debug('PIN TRIES EXCEEDED');
        } else {
            sessionInfo.response = [EverestConfig.EverestVariables.maxPinTries - sessionInfo.pinTries];
            updateUser('ACM-02', 'Input PIN invalid.');
            logger.debug('PIN INVALID');
        }
    }
    function updateUser(responseCode, responseString) {
        sessionInfo.input = 'MASKED FOR SECURITY!';
        sessionInfo.modelName = 'UserChannelOrganizationAccess';
        sessionInfo.queryType = 'update';
        sessionInfo.conditions = {
            identifier: sessionInfo.identifier,
            organizationID: sessionInfo.organizationID,
            channel: sessionInfo.channel
        };

        sessionInfo.responseCode = responseCode;
        sessionInfo.responseString = responseString;
        if (sessionInfo.shouldReIssuePin == 'YES' && sessionInfo.responseCode == 'ACM-00') {
            sessionInfo.responseCode = 'ACM-05'
        }
        callback(sessionInfo);
    }
};

