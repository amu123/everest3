var modConfig           = require('./moduleHandler/ModuleConfig'),
    log4js              = require('log4js'),
    logger              = log4js.getLogger('TRANSACTION HANDLER: ');

exports.processRequest  = function(sessionInfo, callback){
    var accounts = JSON.parse(sessionInfo.accounts);
    var accountSelection = sessionInfo.input;
    sessionInfo.accountNumber = accounts[accountSelection - 1].accountNumber;

    var message              = modConfig.module.transactionLoan.amntMessage;
    sessionInfo.toAcc        = sessionInfo.accountNumber;
    logger.info("***************TO ACCOUNT NUMBER.**********************", sessionInfo.toAcc);
    sessionInfo.responseCode = 'LTRAN-01';
    sessionInfo.response     = [message];
    callback(sessionInfo);
};