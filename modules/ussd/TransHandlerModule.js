var modConfig           = require('./moduleHandler/ModuleConfig');
exports.processRequest  = function(sessionInfo, callback){
    var accounts = JSON.parse(sessionInfo.accounts);
    var accountSelection = sessionInfo.input;
    sessionInfo.accountNumber = accounts[accountSelection - 1].accountNumber;

    var message              = modConfig.module.transaction.amntMessage;
    sessionInfo.toAcc        = sessionInfo.accountNumber;
    sessionInfo.responseCode = 'INTRA-00';
    sessionInfo.response     = [message];
    callback(sessionInfo);
};