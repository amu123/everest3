var log4js = require('log4js');
var DatabaseAccess = require('./connector/DatabaseAccessModule'),
    esbAccess      = require('./connector/EsbAccessModule'),
    waterfall      = require('async-waterfall'),
    EverestConfig = require('./config/EverestConfig');

exports.processRequest = function (sessionInformation, callback) {
    var msisdn = sessionInformation.msisdn;
    var logger = log4js.getLogger('VERIFY CUSTOMER ' + msisdn);
    logger.setLevel('INFO');

    var organizationID = JSON.parse(sessionInformation.organizationID);

    waterfall([
        function(callback){
            esbAccess.createSession(sessionInformation, function (sessionInfo, body) {
                var token = body.token;
                sessionInformation.token=token;
                if(sessionInfo){
                    callback(null,token);
                }
            })
        },
        function(token, callback){
            esbAccess.login(sessionInformation, token, function(err, loginData, respData){
                if(err){
                    logger.fatal(err.stack);
                }else{
                    callback(null, respData.code, respData.message + organizationID);
                }
            })
        },
        function(responseCode, responseString, callback){
            if(sessionInformation.error){
                logger.error(sessionInformation.error.stack);
                sessionInformation.error=null;
            }
            sessionInformation.responseCode=responseCode;
            sessionInformation.responseString=responseString;
            callback(null, sessionInformation);
        }
    ], function(err, result){
        callback(result);
    });
};