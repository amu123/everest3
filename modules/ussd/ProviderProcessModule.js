var ESBAccess       = require('./connector/EsbAccessModule'),
    log4js          = require('log4js'),
    logger          = log4js.getLogger('PROCESS PROVIDER MENU: ');
exports.processRequest=function(sessionInfo, callback){
    var option          = sessionInfo.input;
    var transIDArr      = sessionInfo.MNOID.split(',');
    logger.info('ProviderProcessModule: HAS BEEN FOUND: ', transIDArr + ' | ' + transIDArr.length + ' | ' + sessionInfo.msisdn);
    if(transIDArr.length >= 1){
        if(option <= transIDArr.length && option != 0){
            sessionInfo.transID = transIDArr[option -1];
            ESBAccess.transaction(sessionInfo, function(data){
                if(data){
                    var amnt = data.data.products;
                    var displayString='';
                    sessionInfo.AmntData = JSON.stringify(amnt);
                    sessionInfo.responseCode = "AIRP-00";
                    for(var x = 0; x < amnt.length; x++){
                        displayString = displayString.concat(x + 1 + '.) ').concat(sessionInfo.Currency + ' ' + amnt[x].amount).concat('\n');
                        sessionInfo.response = [displayString];
                    }
                    callback(sessionInfo);
                }
            });
        }else{
            logger.fatal('ERROR: INCORRECT OPTION SELECTED.');
        }
    };
};