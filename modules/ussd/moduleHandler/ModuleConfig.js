exports.module={
    statement:{
        name:'MiniStatementModule',
        code:1,
        message:'Please Select Account: ',
        noTransMessage: 'No Transactions Recorded ',
        invalidAccType: 'Statements are not permitted for this account '
    },
    airtime:{
        name:'AirtimePurchaseModule',
        code:2,
        msisdnPrompt:'Enter Valid Phone Number:',
        message:'Please Select Account: ',
        invalidMsisdn:'Invalid Phone Number.',
        options:{
            myself:'Recharge Me',
            someone:'Recharge Someone',
            message:'Select Purchase Option: '
        }
    },
    transaction:{
        name:'TransactionModule',
        code:3,
        message:'Select From Account: ',
        toMessage:'Select To Account: ',
        amntMessage:'Enter MXN Amount:',
        options:{
            intra:'Transfer Between My Accounts',
            inter:'Transfer To Account',
            mobile:'Send To Mobile ',
            loan:'Loan Payment',
            back:'Return To Main Menu'
        },
        transModule:{
            name:'TransHandlerModule',
            code:'4'
        }
    },
    transactionInter:{
        name:'TransactionInter',
        code:'5',
        message:'Select From Account: ',
        toMessage:'Enter Account Number: ',
        amntMessage:'Enter MXN Amount:',
        confirm:'00',
        confirmMessage:'You want to transfer strAmnt to strAcc, ref: strRef: Compartomus Transfer Please enter 0 to confirm.',
        confirmIntraMessage:'You want to transfer strAmnt to strAcc: Compartomus Transfer Please enter 0 to confirm.'
    },
    transactionMobile:{
        name:'TransactionMobile',
        code:'6',
        message:'Select From Account: ',
        toMessage:'Enter Mobile Number: ',
        amntMessage:'Enter MXN Amount:',
        search: 'MSISDN'
    },
    transactionLoan:{
        name:'TransactionLoan',
        code:'7',
        message:'Select From Account: ',
        toMessage:'Select To Account: ',
        amntMessage:'Enter MXN Amount:'
    },
    billPayment:{
        name:'BillPayment',
        code:'8',
        message:'Select From Account: ',
        billMsg: 'Select Product: ',
        successMsg: 'Bill Payment Successfull. '
    },
    bckMessage: 'Press 9 to go back',
    techError: 'Technical error has occured. Please contact Compartomus support. ',
    amntPrompt: 'Enter MXN Amount:',
    centsMsg: ' Enter amount in cents.',
    oneAccMsg : 'Invalid Option. You need to have to or more Accounts. '
};