/**
 * Created by ntokozo on 2016/07/29.
 */
exports.processRequest=function(name, callback){
    if(name){
        callback(require('../' + name));
    }else{
        callback(name);
    }  
};