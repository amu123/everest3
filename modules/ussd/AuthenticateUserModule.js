/**
 * Created by ntokozo on 2016/07/13.
 */
exports.processRequest = function (sessionInformation, callback) {
    if(sessionInformation.encryptedPin){
        sessionInformation.input = 'MASKED FOR SECURITY!';
        sessionInformation.status = 'SUCCESS';
        sessionInformation.responseCode = 'AUM-00';
        sessionInformation.responseString = 'User authenticated.';
        callback(sessionInformation);
    }else{
        sessionInformation.input = 'MASKED FOR SECURITY!';
        sessionInformation.status = "FAILURE";
        sessionInformation.responseCode = 'AUM-02';
        sessionInformation.responseString = 'User not authenticated.';
        callback(sessionInformation);
    }
};