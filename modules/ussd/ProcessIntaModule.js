var esbAccess   = require('./connector/EsbAccessModule');
var modConfig   = require('./moduleHandler/ModuleConfig');

exports.processRequest=function(sessionInfo, callback){
    var cents = sessionInfo.input;
    sessionInfo.amount = sessionInfo.fullAmnt + '.' + cents;
    sessionInfo.ref = 'REF';
    sessionInfo.searchData = 'ACCOUNT';
    sessionInfo.value = sessionInfo.toAcc;
    esbAccess.transaction(sessionInfo, function(transferResponse){
        var message = '';
        if(transferResponse){
            var _code  = transferResponse.data.code;
            var _ref   = transferResponse.data.everest_ref;
            var amount = sessionInfo.Currency + ' ' + sessionInfo.amount;
            message = modConfig.module.transactionInter.confirmMessage +'\n'+ modConfig.module.bckMessage;
            var mapObj = {strAmnt:amount, strAcc:sessionInfo.toAcc, strRef:_ref};
            message = message.replace(/strAmnt|strAcc|strRef/gi, function (matched) {
                return mapObj[matched];
            });
            switch (_code){
                case '00':
                    sessionInfo.everestRef = transferResponse.data.everest_ref;
                    sessionInfo.responseCode='ESBA-00';
                    sessionInfo.response = [message];
                    callback(sessionInfo);
                    break;
                case '49':
                    sessionInfo.responseCode='ESBA-01';
                    sessionInfo.response = [message];
                    callback(sessionInfo);
                    break;
            }
        }else{
            sessionInfo.responseCode='UBM-01';
            sessionInfo.response = ['Transaction.Failed'];
            callback(sessionInfo);
        }
    });
};