var log4js = require('log4js'),
    SessionManager = require('./connector/SessionManagerModule'),
    UssdMenuManager = require('./connector/UssdManagerModule'),
    DatabaseAccess = require('./connector/DatabaseAccessModule'),
    EverestConfig = require('./config/EverestConfig');


var USSDSession = function () {

    var logger;

    this.processSession = function (ussdInfo, callback) {
        var msisdn = ussdInfo.msisdn;
        logger = log4js.getLogger('USSD Session: ' + msisdn);
        logger.setLevel('DEBUG');
        logger.info('REQUEST');

        ussdInfo.channel = 'USSD';
        ussdInfo.identifier = msisdn;
        ussdInfo.sessionIdentifier = 'USSD:' + msisdn;
        try {
            fetchSessionInfo(ussdInfo, callback);
        } catch (e) {
            processError(ussdInfo, e, callback);
        }
    };

    function fetchSessionInfo(ussdInfo, callback) {
        logger.debug('FETCHING SESSION INFO');
        SessionManager.fetchSessionInfo(ussdInfo, function (error, sessionInfo) {
            if (error) {
                processError(ussdInfo, error, callback);
            } else {

                switch (ussdInfo.requestType) {
                    case '1':
                        deleteSession(ussdInfo, function () {
                            fetchUssdProperties(ussdInfo, callback);
                        });
                        break;
                    case '2':
                        // In the event of a 2 & 3 coming in at the same time
                        if (!sessionInfo) {
                            processError(ussdInfo, new Error('A disconnection has occurred at the Mobile Network Operator level. Please redial the USSD service code.'), callback);

                        } else {
                            sessionInfo.input = ussdInfo.input;
                            if (sessionInfo.pageIndex == -1) {
                                processPosition(sessionInfo, callback);
                            } else {
                                var displayArray = JSON.parse(sessionInfo.displayArray);
                                switch (sessionInfo.input) {
                                    case '**':
                                        if (sessionInfo.pageIndex == 0) {
                                            sessionInfo.pageIndex = displayArray.length - 1;
                                        } else {
                                            sessionInfo.pageIndex = --sessionInfo.pageIndex;
                                        }
                                        sessionInfo.display = displayArray[sessionInfo.pageIndex];
                                        sessionInfo.resumeDisplay = displayArray[sessionInfo.pageIndex];
                                        updateSession(sessionInfo, callback);
                                        break;
                                    case '##':
                                        if (sessionInfo.pageIndex == displayArray.length - 1) {
                                            sessionInfo.pageIndex = 0;
                                        } else {
                                            sessionInfo.pageIndex = ++sessionInfo.pageIndex;
                                        }
                                        sessionInfo.display = displayArray[sessionInfo.pageIndex];
                                        sessionInfo.resumeDisplay = displayArray[sessionInfo.pageIndex];
                                        updateSession(sessionInfo, callback);
                                        break;
                                    default:
                                        processPosition(sessionInfo, callback);
                                        break;
                                }

                            }
                        }
                        break;
                    case '3':
                        deleteSession(sessionInfo, callback);
                        break;
                }
            }
        })
    }

    function fetchUssdProperties(ussdInfo, callback) {
        logger.debug('FETCHING USSD PROPERTIES');
        UssdMenuManager.fetchUssdProperties(ussdInfo, function (ussdInfo) {
            var ussdProperty = ussdInfo.ussdProperty;
            if (!ussdProperty) {
                ussdInfo.display = 'Unsupported USSD short code. Please contact WIZZIT on 0861 949 948';
                ussdInfo.continueSession = false;
                callback(ussdInfo);
            } else {
                if (ussdProperty.isOnMaintenance) {
                    ussdInfo.continueSession = false;
                    ussdInfo.display = 'Service temporarily down, we apologise for the inconvenience. For any additional information please contact WIZZIT on 0861 949 948.';
                    respondToCallback(ussdInfo, callback);
                } else {
                    ussdInfo.organizationID = ussdProperty.organizationID;
                    ussdInfo.ussdMenu = ussdProperty.ussdMenu;
                    ussdInfo.positionID = 'start';
                    processPosition(ussdInfo, callback);
                }
            }
        })
    }

    function processPosition(sessionInfo, callback) {
        logger.debug('PROCESSING POSITION');
        UssdMenuManager.processPosition(sessionInfo, function (sessionInfo) {
            fetchPositionProperties(sessionInfo, callback);
        })
    }

    function fetchPositionProperties(sessionInfo, callback) {
        UssdMenuManager.fetchPositionProperties(sessionInfo, function (sessionInfo) {
            var maxPageSize = EverestConfig.EverestVariables.maxPageSize;
            if (sessionInfo.display.length > maxPageSize) {
                var footer = UssdMenuManager.UssdMenuCollection[sessionInfo.ussdMenu].pagingFooter[sessionInfo.language];
                var pageSize = maxPageSize - footer.length;
                var display = sessionInfo.display;
                var displayArray = [];
                logger.debug(display.length);
                for (var i = 0, j = 0; i < display.length; i += pageSize, j++) {
                    displayArray[j] = display.substring(i, i + pageSize).concat('\n' + footer);
                    if (i > display.length - pageSize) {
                        sessionInfo.display = displayArray[0];
                        sessionInfo.resumeDisplay = displayArray[0];
                        sessionInfo.displayArray = JSON.stringify(displayArray);
                        sessionInfo.pageIndex = 0;
                        updateSession(sessionInfo, callback);
                    }
                }
            } else {
                sessionInfo.pageIndex = -1;
                sessionInfo.displayArray = null;
                updateSession(sessionInfo, callback);
            }
        })
    }

    function updateSession(sessionInfo, callback) {
        logger.debug('UPDATING SESSION INFO');
        sessionInfo.expire = EverestConfig.EverestVariables.expireSession;
        SessionManager.updateSession(sessionInfo, function (error, reply) {
            if (error) {
                processError(sessionInfo, error, callback);
            } else {
                logger.debug(reply);
                respondToCallback(sessionInfo, callback);
            }
        });
    }

    function deleteSession(sessionInfo, callback) {
        logger.debug('DELETING SESSION');
        SessionManager.deleteSession(sessionInfo, function (error, reply) {
            if (error) {
                logger.error(error.stack);
            } else {
                logger.debug(reply);
                callback(sessionInfo);
            }
        });
    }

    function processError(sessionInfo, error, callback) {
        logger.debug('PROCESSING ERROR');
        logger.error(error.stack);
        sessionInfo.display = error.message;
        sessionInfo.continueSession = false;
        respondToCallback(sessionInfo, callback);
    }

    function respondToCallback(sessionInfo, callback) {
        logger.debug(sessionInfo.display);
        logger.info('RESPONDING');
        callback(sessionInfo);
        var databaseParameters = {
            modelName: 'UssdTransaction',
            queryType: 'save',
            instanceInfo: {
                transactionLogDate: new Date(),
                userMsisdn: sessionInfo.msisdn,
                userRequest: sessionInfo.input,
                systemResponse: sessionInfo.display,
                organizationID: sessionInfo.organizationID,
                fullRef:sessionInfo.fullRef
            }
        };
        DatabaseAccess.processRequest(databaseParameters, function (result) {
            if (result.error) {
                logger.error('An error occurred writing USSD transaction data!');
                logger.error(result.error.statck);
            }
        })
    }
};

module.exports = USSDSession;
