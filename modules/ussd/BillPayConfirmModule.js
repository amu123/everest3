/**
 * Created by ntokozo on 2016/09/09.
 */
var modConfig = require('./moduleHandler/ModuleConfig');
exports.processRequest  = function(sessionInfo, callback){
    sessionInfo.amount = sessionInfo.fullAmnt + '.' + sessionInfo.input;
    sessionInfo.toAcc = sessionInfo.accountNumber;
    var message = modConfig.module.transactionInter.confirmIntraMessage;
    var mapObj = {strAmnt:sessionInfo.amount, strAcc:sessionInfo.toAcc};
    message = message.replace(/strAmnt|strAcc/gi, function (matched) {
        return mapObj[matched];
    });
    sessionInfo.responseCode = 'BPCM-11';
    sessionInfo.response = [message];
    callback(sessionInfo);
};