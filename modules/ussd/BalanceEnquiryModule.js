var log4js = require('log4js'),
    esbAccess       = require('./connector/EsbAccessModule'),
    logger = log4js.getLogger('BALANCE ENQUIRY: ');

exports.processRequest = function (sessionInfo, callback) {
   sessionInfo.fromAcc = sessionInfo.accountNumber;
   esbAccess.transaction(sessionInfo, function(data){
      if(data.code === '08'){
         sessionInfo.responseCode   = 'BEM-07';
         sessionInfo.currentBalance = 'A Technical Error Has Occured. Please Contact Compartomous Support. ';
         callback(sessionInfo);
      }else if(data.code === '00'){
         var balances = data.data.balance_result;
         var displayBalance = [];
         for(var x = 0; x < balances.length; x++){
            displayBalance.push(balances[x].name + ' ' + balances[x].currency + ' ' + balances[x].balance + ' ');
         }
         logger.info('DATA: ', displayBalance);
         var accounts = data;
         sessionInfo.responseCode = 'BEM-00';
         sessionInfo.availableBalance=displayBalance;
         sessionInfo.currentBalance=data.data.balance_result[0].balance;
         callback(sessionInfo);
      }
   });
};
