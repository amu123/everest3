/**
 * Created by ntokozo on 2016/09/08.
 */
var modConfig   = require('./moduleHandler/ModuleConfig');
var esbAccess   = require('./connector/EsbAccessModule');
exports.processRequest = function(sessionInfo, callback){
    var input = sessionInfo.input;
    switch (input){
        case '0':
            sessionInfo.serviceaccount = '11223344';
            var option = sessionInfo.input;
            esbAccess.transaction(sessionInfo, function(esbData){
                var message = '';
                if(esbData){
                    var _code = esbData.code;
                    switch (_code){
                        case '00':
                            message = modConfig.module.billPayment.successMsg + ' ' + modConfig.module.bckMessage;
                            sessionInfo.responseCode = 'SBPM-00';
                            sessionInfo.response = [message];
                            callback(sessionInfo);
                            break;
                    }
                }
            });
            break;
    }
};