var redis = require('redis'),
    log4js = require('log4js'),
    Config = require('../config/SessionManagerConfig');

var logger = log4js.getLogger('SESSION MANAGER');
logger.setLevel(Config.loggerLevel);

function createRedisClient() {
    return redis.createClient(Config.redisPort, Config.redisHostIP, Config.redisOptions);
}
var redisClient = createRedisClient();

redisClient.on('connected', function () {
    logger.debug('REDIS CONNECTED');
});

redisClient.on('ready', function () {
    logger.info('READY');
});

redisClient.on('error', function (error) {
    logger.error('AN ERROR OCCURRED CONNECTING TO REDIS');
    if (error) {
        throw error;
    }
    if (redisClient) {
        redisClient.removeAllListeners();
        redisClient.quit();
    }
});

redisClient.on('end', function () {
    logger.warn('REDIS CONNECTION CLOSED');
    redisClient = createRedisClient();
});

redisClient.on('drain', function () {
    logger.debug('DRAIN');
});

redisClient.on('idle', function () {
    logger.debug('IDLE');
});

exports.fetchSessionInfo = function (sessionInfo, callback) {
    redisClient.hgetall(sessionInfo.sessionIdentifier, function (redisGetError, redisGetReply) {
        if (redisGetError) {
            console.log('ERROR: ', redisGetError);
            callback(redisGetError, null);
        } else {
            callback(null, redisGetReply);
        }
    });
};

exports.updateSession = function (sessionInfo, callback) {
    // In the event of a 2 & 3 coming in at the same time
    if (!sessionInfo) {
        callback(new Error('A disconnection has occurred at the Mobile Network Operator level. Please redial the USSD service code.'), null);
    } else {
        var key = sessionInfo.sessionIdentifier;
        logger.info('UPDATING SESSION: ' + key);
        if (sessionInfo.continueSession) {
            var expire = sessionInfo.expire;
            redisClient.hmset(key, sessionInfo, function (redisSetError, redisSetReply) {
                if (redisSetError) {
                    callback(redisSetError, null);
                } else {
                    sessionInfo.latest = redisSetReply;
                    callback(null, redisSetReply);
                }
            });
            if (expire) {
                expireSession(key, expire);
            }
        } else {
            redisClient.del(key, function (redisDeleteError, redisDeleteReply) {
                if (redisDeleteError) {
                    logger.error(redisDeleteError.stack);
                    callback(null, sessionInfo);
                } else {
                    logger.debug(redisDeleteReply);
                    callback(null, sessionInfo);
                }
            })
        }
    }
};

exports.deleteSession = function (sessionInfo, callback) {
    // In the event of a 2 & 3 coming in at the same time
    if (!sessionInfo) {
        return;
    }
    var key = sessionInfo.sessionIdentifier;
    logger.info('DELETING SESSION: ' + key);
    redisClient.del(key, function (redisDeleteError, redisDeleteReply) {
        if (redisDeleteError) {
            callback(redisDeleteError, null);
        } else {
            logger.debug(redisDeleteReply);
            callback(null, redisDeleteReply);
        }
    })
};

function expireSession(key, expire) {
    redisClient.expire(key, expire, function(redisExpireError, redisExpireReply) {
        if (redisExpireError) {
            logger.error(redisExpireError.stack);
        } else {
            logger.debug('EXPIRE REPLY: ' + redisExpireReply);
        }
    })
}
