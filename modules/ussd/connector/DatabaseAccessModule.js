/**
 * Created by ntokozo on 2016/07/08.
 */
var mongoose = require('mongoose'),
    log4js = require('log4js');
//require('../../configuration/domain/model/UssdTransactionModel')
var logger = log4js.getLogger('DATABASE ACCESS');
logger.setLevel('INFO');
exports.processRequest = function (sessionInformation, callback) {
    var Model = require('../config/domain/model/' + sessionInformation.modelName + 'Model');
    switch (sessionInformation.queryType) {
        case 'findOne':
            logger.info('MODEL: ', sessionInformation.modelName+'Model' + ' CONDITIONS: ', sessionInformation.conditions);
            org=sessionInformation.conditions.organizationID;
            Model.findOne(sessionInformation.conditions, function (error, record) {
                logger.info('RECORD: ', record);
                callCallback(error, record);
            });
            break;
        case 'find':
            Model.find(sessionInformation.conditions, function (error, records) {
                callCallback(error, records);
            });
            break;
        case 'save':
            var modelInstance = new Model(sessionInformation.instanceInfo);
            modelInstance.save(function (error, record) {
                callCallback(error, record);
            });
            break;
        case 'update':
            Model.update(sessionInformation.conditions, sessionInformation.update, {multi: true}, function (error, numberAffected, response) {
                callCallback(error, response, numberAffected);
            });
            break;
        default: // This must not occur!
            sessionInformation.error = new Error('Unsupported query type');
            callback(sessionInformation);
            break;
    }

    function callCallback(error, information, additionalInformation) {
        if (callback) {
            if (error) {
                sessionInformation.error = error;
                callback(sessionInformation);
            } else {
                sessionInformation.information = information;
                sessionInformation.additionalInformation = additionalInformation;
                callback(sessionInformation);
            }
        }
    }
};
