var request     = require('request'),
    dbAccess    = require('./DatabaseAccessModule'),
    transLogger = require('../UssdTransactionLogger'),
    bodyParser  = require('body-parser'),
    log4js      = require('log4js');

var esbQuery=function(){};

var logger = log4js.getLogger('ESB ACCESS');
logger.setLevel('INFO');

esbQuery.prototype.createSession=function(sessionInfo,callback){
    try{
        request({
            url:'http://10.5.2.1:9091/ESBConnector/createSession',
            method:'POST',
            json:{
                channel         : "APP",
                organizationID  : "COMPARTAMOS",
                deviceId        : "EA36CC99-D77B-47B7-BBBC-FD7051396AD9", //"USSD-DEVICE",
                application     : "CMPXA001"
            }
        }, function(err, response, body){
            if(err){
                callCallback('VCM-05', 'A technical error has occurred accessing user records!')
            }else if(!body){
                callCallback('VCM-06', 'Record information missing in users!');
            }else{
                logger.info('BODY: ', body);
                sessionInfo.language = "english";
                sessionInfo.status = 'SUCCESS';
                sessionInfo.encryptedPin='1234';
                sessionInfo.pinTries = 0;
                sessionInfo.onUsSpent = 0;
                sessionInfo.notOnUsSpent = 0;
                sessionInfo.onUsLimit = 5000;
                sessionInfo.notOnUsLimit = 5000;
                callback(sessionInfo, body);
            }
        })
    }catch(e){
        logger.fatal(e.stack);
    }
};
esbQuery.prototype.login=function(sessionInfo, token, callback){
    var token={
        "x-access-token":token
    };
    try{
        request({
            url:'http://10.5.2.1:9091/ESBConnector/service',
            headers: token,
            method:'POST',
            json:{
                action          :"LOGIN",
                channel         :"APP",
                application     :"CMPXA001",
                organizationID  :"COMPARTAMOS",
                username        :sessionInfo.msisdn,
                password        :"1234",
                deviceId        : "EA36CC99-D77B-47B7-BBBC-FD7051396AD9" //"USSD-DEVICE"
            }
        }, function(error, response, body){
            var respData;
            if(error){
                logger.fatal(error.stack);
                respData=({code:'VCM-05', message:'A technical error has occurred accessing user records!'});
                callback(null, null, respData);
            }else if(!body){
                respData=({code:'VCM-06', message:'Record information missing in users!'});
                callback(null, null, respData);
            }else{
                respData=({code:'VCM-00', message:'MSISDN has active accounts for organization: '});
                var accounts = body.data.accounts;
                var accountDisplay = '';
                for (var i = 0; i < accounts.length; i++) {
                    accountDisplay = accountDisplay.concat(i + 1 + '. ').concat(accounts[i].accountNumber).concat('\n');
                    if (i == accounts.length - 1) {
                        sessionInfo.accountDisplay = accountDisplay;
                    }
                }
                sessionInfo.name = body.data.user.firstName;
                sessionInfo.surname = body.data.user.lastName;
                sessionInfo.msisdn = body.data.user.msisdn;
                sessionInfo.accounts=JSON.stringify(accounts);
                callback(null,body,respData);
            }
        })
    }catch (e){
        logger.fatal(e.stack);
    }
};
esbQuery.prototype.transaction=function(sessionInfo, callback){
    logger.info('SESSION INFO: ESB ACCESS: ', sessionInfo.trans_type);
    var token={
        "x-access-token":sessionInfo.token
    };
    try{
        request({
            url:'http://10.5.2.1:9091/ESBConnector/esb/core/transaction',
            headers:token,
            method:'POST',
            json:{
                account         :sessionInfo.fromAcc,
                trans_type      :sessionInfo.trans_type,
                agentid         :'normalCustomer',
                //============AIRTIME!!
                provider_id     :sessionInfo.transID,
                everest_ref     :sessionInfo.everestRef,
                product_id      :sessionInfo.product_id,
                amount          :sessionInfo.amount,
                topupmsisdn     :sessionInfo.topupmsisdn,
                //============TRANSFER!!
                toAccount       :sessionInfo.toAcc,
                reference       :sessionInfo.ref,
                //============INTERTRANSFER!!
                search          : sessionInfo.searchData,
                //value           : sessionInfo.toAcc,
                value           : sessionInfo.value,
                tranTypeGroup   :"INTERNAL_TRANSFER_GROUP",
                //============LOANRANSFER!!
                toaccountnumber :sessionInfo.toAcc,
                customer_confirmation:sessionInfo.confirm,
                //============SERVICEBILLPAYMENT!!
                serviceaccount  : sessionInfo.serviceaccount,
                sku             : sessionInfo.sku
            }
        }, function (error, response, body) {
            if(error){
                callback(error);
            }else if(!body){
                respData=({code:'VCM-06', message:'Record information missing in users!'});
                callback(respData);
            }else{
                logger.info('THE BODY: ', body);
                transLogger.logTrans(sessionInfo, body, function (loggedData) {
                    callback(body);
                });
            }
        })
    }catch (e){
        logger.fatal(e.stack);
    }
};

module.exports=new esbQuery();