/**
 * Created by ntokozo on 2016/07/08.
 */
/**
 * Created by ntokozo on 2016/06/22.
 */
var fs = require('fs'),
    log4js = require('log4js'),
    async = require('async'),
    path = require('path'),
    Config = require('../config/UssdMenuManagerConfig');

var jsonPath = path.join(__dirname, Config.propertyPath);

var logger = log4js.getLogger(Config.loggerName);
logger.setLevel(Config.loggerLevel);


var ussdProperties;
var UssdMenuCollection = {};

exports.UssdMenuCollection = UssdMenuCollection;

var encoding = Config.encoding;

console.log('CONFIG PATH: ', jsonPath);

fs.readFile(jsonPath, encoding, function (error, data) {
    if (error) {
        logger.fatal(error.stack);
        throw error;
    } else {
        logger.info('LOADING USSD SHORT CODES');
        ussdProperties = JSON.parse(data);
    }
});

var menuPath = path.join(__dirname, Config.menuPath);
//var menuPath='/home/ntokozo/Development/esb_ussd/core/configuration/Ussd/Menu';
fs.readdir(menuPath, function (error, result) {
    if (error) {
        logger.fatal(error.stack);
        throw error;
    } else {
        result.forEach(function (fileName) {
            fs.readFile(menuPath + '/' + fileName, encoding, function (error, data) {
                if (error) {
                    logger.fatal(error.stack);
                    throw error;
                } else {
                    logger.info('LOADING USSD MENU: ' + fileName);
                    var everestUssdMenu = JSON.parse(data);
                    UssdMenuCollection[everestUssdMenu.id] = everestUssdMenu.menu;
                    UssdMenuCollection[everestUssdMenu.id].sms = everestUssdMenu.sms;
                    UssdMenuCollection[everestUssdMenu.id].pagingFooter = everestUssdMenu.pagingFooter;
                }
            });
        })
    }
});

exports.fetchUssdProperties = function (ussdInfo, callback) {
    logger.debug('FETCH USSD PROPERTIES');
    var _shortCode = ussdInfo.requestMessage;

    for (var i = 0; i < ussdProperties.routes.length; i++) {
        var route = ussdProperties.routes[i];
        if (_shortCode.slice(0, route.shortCode.length) == route.shortCode) {
            ussdInfo.ussdProperty = route;
            if (_shortCode.length > route.shortCode.length) {
                ussdInfo.shortCutSteps = _shortCode.replace(route.shortCode + '*', '');
            }
            callback(ussdInfo);
            break;
        }
        if (i == ussdProperties.routes.length - 1) {
            callback(ussdInfo);
        }
    }
};

exports.fetchPositionProperties = function (ussdInfo, callback) {
    try {
        var positionID = ussdInfo.positionID;
        var display = '';
        var ussdMenu = ussdInfo.ussdMenu;
        var language = ussdInfo.language;
        var position = UssdMenuCollection[ussdMenu][positionID];
        ussdInfo.continueSession = position.continueSession;
        var items = position.items;
        if (items) {
            for (var i = 0, j = 1; i < items.length; i++) {
                var item = items[i];
                if (item.isEnabled) {
                    if (item.option) {
                        if (item.option.text.multiLanguage) {
                            language = 'multiLanguage';
                        }
                        display = display.concat(j++ + '.' + item.option.text[language]);
                    } else {
                        if (item.text.multiLanguage) {
                            language = 'multiLanguage';
                        }
                        display = display.concat(item.text[language]);
                    }
                    display = display.concat('\n');
                }

                if (i == items.length - 1) {
                    buildDisplay();
                }
            }
        } else {
            if (position.text.multiLanguage) {
                language = 'multiLanguage';
            }
            display = position.text[language];
            buildDisplay();
        }
    } catch (e) {
        ussdInfo.display = 'A technical error has occurred. Please contact WIZZIT on 0861 949948';
        ussdInfo.continueSession = false;
        logger.error(e.stack);
        callback(ussdInfo);
    }

    function buildDisplay() {
        if (ussdInfo.response) {
            for (var k = 0; k < ussdInfo.response.length; k++) {
                display = display.replace('@', ussdInfo.response[k]);
                if (k == ussdInfo.response.length - 1) {
                    ussdInfo.display = display.trim();
                    ussdInfo.resumeDisplay = display.trim();
                    callback(ussdInfo);
                }
            }
        } else {
            ussdInfo.display = display.trim();
            ussdInfo.resumeDisplay = display.trim();
            callback(ussdInfo);
        }
    }
};

/*
 Process the current USSD position
 */

exports.processPosition = processPosition;

function processPosition(ussdInfo, callback) {
    var ussdMenu = ussdInfo.ussdMenu;
    var positionID = ussdInfo.positionID;
    logger.debug('PROCESS POSITION ' + positionID);

    var position = UssdMenuCollection[ussdMenu][positionID];

    ussdInfo.allowShortCut = position.allowShortCut;

    var items = position.items;
    var module = position.module;

    // Currently in menu
    if (items) {
        var isFound = false;
        for (var i = 0, j = 0; i < items.length; i++) {
            var item = items[i];
            // Menu item matched
            if (item.isEnabled && item.option && ussdInfo.input == ++j) {
                isFound = true;
                // Save any properties of the item to the session information
                var properties = item.properties;
                if (properties) {
                    properties.forEach(function (property) {
                        ussdInfo[property.key] = property.value;
                    });
                }
                if (position.saveKey) {
                    ussdInfo[position.saveKey] = item.saveValue;
                }
                if (item.module) {
                    module = item.module;
                }
                if (!item.module && item.nextPositionID) {
                    module = null;
                }
                // Call module if specified
                if (module) {
                    callModule();
                    // Shift to the next position
                } else {
                    var nextPositionID;
                    if (item.nextPositionID) {
                        nextPositionID = item.nextPositionID;
                    } else {
                        nextPositionID = position.nextPositionID;
                    }
                    ussdInfo.positionID = nextPositionID;
                    ussdInfo.resumePositionID = nextPositionID;
                    if (ussdInfo.shouldProcessShortCut == 'YES') {
                        processShortCut(ussdInfo, callback);
                    } else {
                        callback(ussdInfo);
                    }
                }
            }
            // Input range outside length of menu
            if (i == items.length - 1 && !isFound) {
                ussdInfo.status = 'FAILURE';
                ussdInfo.shortCutSteps = null;
                return callback(ussdInfo);
            }
        }
        // Currently not in menu
    } else {
        // Save any properties of the position to the session information
        properties = position.properties;
        if (properties) {
            properties.forEach(function (property) {
                ussdInfo[property.key] = property.value;
            });
        }
        // Save the input if required
        if (position.saveKey) {
            ussdInfo[position.saveKey] = ussdInfo.input;
        }
        // Call a module if specified
        if (module) {
            callModule();
            // Shift position to next position specified if module not called
        } else {
            nextPositionID = position.nextPositionID;
            if (!nextPositionID) {
                ussdInfo.shortCutSteps = null;
                callback(ussdInfo);
            } else {
                ussdInfo.positionID = nextPositionID;
                ussdInfo.resumePositionID = nextPositionID;
                if (ussdInfo.shouldProcessShortCut == 'YES') {
                    processShortCut(ussdInfo, callback);
                } else {
                    callback(ussdInfo);
                }
            }
        }
    }

    // Call a module before proceeding to the next step
    function callModule() {
        var Module = require('../' + module.name);
        logger.info('FINDING MODULE: ', module.name);
        Module.processRequest(ussdInfo, function (ussdInfo) {
            logger.info('FINDING MODULE RESPONSE: ', module.response);
            module.response.forEach(function (responseItem) {
                // Match module response code to file response code
                // TODO No match
                logger.debug(responseItem.responseCode);
                logger.debug(ussdInfo.responseCode);
                if (responseItem.responseCode == ussdInfo.responseCode) {
                    ussdInfo.positionID = responseItem.nextPositionID;
                    ussdInfo.resumePositionID = responseItem.nextPositionID;
                    // Check for shortcut
                    if (ussdInfo.shouldProcessShortCut == 'YES') {
                        processShortCut(ussdInfo, callback);
                    } else {
                        callback(ussdInfo);
                    }
                }
            })
        })
    }
}

/*
 Function to handle USSD shortcuts
 eg. *120*949*1# for direct balance enquiry
 */

function processShortCut(ussdInfo, _callback) {
    logger.debug('PROCESS SHORTCUT');
    ussdInfo.shouldProcessShortCut = 'NO';
    var shortCutSteps = [];
    var shortCutInputs = ussdInfo.shortCutSteps.split('*');
    for (var i = 0; i < shortCutInputs.length; i++) {
        shortCutSteps[i] = shortCutHandler;
        if (i == shortCutInputs.length - 1) {
            async.waterfall(shortCutSteps,
                function (error) {
                    ussdInfo.shortCutSteps = null;
                    if (error) {
                        logger.warn(error.message);
                    }
                    _callback(ussdInfo);
                });
        }
    }

    function shortCutHandler(callback) {
        ussdInfo.input = shortCutInputs.shift();
        processPosition(ussdInfo, function (ussdInfo) {
            if (ussdInfo.status == 'FAILURE' || !ussdInfo.allowShortCut) {
                callback(new Error('Halting shortcut: ' + ussdInfo.responseString));
            } else {
                callback(null);
            }
        })
    }

    /*async.eachSeries(shortCutInputs, function(shortCutInput, callback) {
     ussdInfo.input = shortCutInput;
     processPosition(ussdInfo, function(ussdInfo) {
     if (ussdInfo.status == 'FAILURE' || !ussdInfo.allowShortCut) {
     callback(new Error('Halting shortcut: ' + ussdInfo.responseString));
     } else {
     callback(null);
     }
     }, function(error) {
     if (error) {
     logger.info(error.message);
     }
     ussdInfo.shortCutSteps = null;
     _callback(ussdInfo);
     });
     })*/
}





