var moduleHandler   = require('./moduleHandler/moduleHandler');
    log4js               = require('log4js');

var logger = log4js.getLogger('ACCOUNT SELECTION: ');

exports.processRequest = function (sessionInformation, callback) {
    var name = sessionInformation.findModule;
    moduleHandler.processRequest(name, function (_module) {
        if(_module){
            sessionInformation.status = 'SUCCESS';
            var accounts = JSON.parse(sessionInformation.accounts);
            var accountSelection = sessionInformation.input;
            sessionInformation.accountNumber = accounts[accountSelection - 1].accountNumber;
            logger.info('*******************************ACC NUMBER:  ', sessionInformation.accountNumber);
            switch(sessionInformation.modCode){
                case '1':
                    sessionInformation.trans_type = accounts[accountSelection - 1].product.tran_types[3].code;
                    _module.processRequest(sessionInformation, function (sessionInformation) {
                        logger.info('*******************************STATEMENT:  ', sessionInformation.miniStatement, ' RESPONSE_CODE: ', sessionInformation.responseCode);
                        sessionInformation.response = [sessionInformation.miniStatement];
                        callback(sessionInformation);
                    });
                    break;
                case '2':
                    logger.info('AIRTIME_PURCHASE');
                    sessionInformation.trans_type = accounts[accountSelection - 1].product.tran_types[0].code;
                    sessionInformation.Currency   = accounts[accountSelection - 1].currency.code;
                    _module.processRequest(sessionInformation, function (sessionInformation) {
                        sessionInformation.response = [sessionInformation.s_provider];
                        callback(sessionInformation);
                    });
                    break;
                case '3':
                    logger.info('INTRA ACCOUNT TRANSFERS');
                    sessionInformation.trans_type = accounts[accountSelection - 1].product.tran_types[5].code;
                    sessionInformation.Currency   = accounts[accountSelection - 1].currency.code;
                    _module.processRequest(sessionInformation, function (sessionInformation) {
                        sessionInformation.response = [sessionInformation.display];
                        callback(sessionInformation);
                    });
                    break;
                case '5':
                    logger.info('INTER ACCOUNT TRANSFERS');
                    sessionInformation.trans_type = accounts[accountSelection - 1].product.tran_types[4].code;
                    sessionInformation.Currency   = accounts[accountSelection - 1].currency.code;
                    _module.processRequest(sessionInformation, function (sessionInformation) {
                        sessionInformation.response = [sessionInformation.display];
                        callback(sessionInformation);
                    });
                    break;
                case '6':
                    logger.info('TRANSFER TO ACCOUNT');
                    sessionInformation.trans_type = accounts[accountSelection - 1].product.tran_types[4].code;
                    sessionInformation.Currency   = accounts[accountSelection - 1].currency.code;
                    _module.processRequest(sessionInformation, function (sessionInformation) {
                        sessionInformation.response = [sessionInformation.display];
                        callback(sessionInformation);
                    });
                    break;
                case '7':
                    logger.info('LOAN PAYMENT');
                    sessionInformation.trans_type = accounts[accountSelection - 1].product.tran_types[6].code;
                    sessionInformation.Currency   = accounts[accountSelection - 1].currency.code;
                    _module.processRequest(sessionInformation, function (sessionInformation) {
                        sessionInformation.response = [sessionInformation.display];
                        callback(sessionInformation);
                    });
                    break;
                case '8':
                    logger.info('BILL PAYMENT');
                    sessionInformation.trans_type = accounts[accountSelection - 1].product.tran_types[2].code;
                    sessionInformation.Currency   = accounts[accountSelection - 1].currency.code;
                    _module.processRequest(sessionInformation, function (sessionInformation) {
                        sessionInformation.response = [sessionInformation.display];
                        callback(sessionInformation);
                    });
                    break;
            }
        }else{
            callback({code:'06', message:'fail'});
        }
    });
};
