var preAir      = require('./PreAirtimeModule');
var log4js      = require('log4js');
var logger      = log4js.getLogger('MSISDN PROMPT');
exports.processRequest = function (sessionInformation, callback){
    var toMsisdn  = sessionInformation.input;
    logger.info('*****************MSISDN*****************', toMsisdn.length);
    if(toMsisdn.length < 10){
        sessionInformation.input = '3';
        preAir.processRequest(sessionInformation, function(sessionInfo){
            callback(sessionInfo);
        });
    }else if(toMsisdn.length >= 10){
        sessionInformation.input = '1';
        sessionInformation.toMsisdn = toMsisdn;
        preAir.processRequest(sessionInformation, function(sessionInfo){
            callback(sessionInfo);
        });
    }
};