var BalanceEnquiryModule = require('./BalanceEnquiryModule'),
    log4js               = require('log4js');

var logger = log4js.getLogger('ACCOUNT SELECTION: ');

exports.processRequest = function (sessionInformation, callback) {
    sessionInformation.status = 'SUCCESS';
    var accounts = JSON.parse(sessionInformation.accounts);
    var accountSelection = sessionInformation.input;
    sessionInformation.accountNumber = accounts[accountSelection - 1].accountNumber;
    sessionInformation.trans_type = accounts[accountSelection - 1].product.tran_types[1].code;
    BalanceEnquiryModule.processRequest(sessionInformation, function (sessionInformation) {
        sessionInformation.response = [sessionInformation.availableBalance, sessionInformation.currentBalance, sessionInformation.accountName];
        callback(sessionInformation);
    })
};
