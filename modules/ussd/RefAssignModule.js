/**
 * Created by ntokozo on 2016/07/26.
 */
var chance      = require('chance'),
    log4js      = require('log4js'),
    dateFormat  = require('dateformat');
    randomstr   = require('randomstring'),
    dbAccess    = require('./connector/DatabaseAccessModule'),
    stmtLogger  = require('./config/domain/model/StatementLoggerModel');

var logger      = log4js.getLogger('REF ASSIGN MODULE: ');

exports.processRequest=function(statements, sessionInfo, callback){
    var instanceInfo            = statements;
    var random;

    for(var x=0; x < statements.length; x++){
        random                          = randomstr.generate({length:6, charset:'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890'});
        instanceInfo[x].shortRef        = random;
    }

    var stmt                    = new stmtLogger();
    stmt.statement              = instanceInfo;
    stmt.save(function(err, data){
        if(!err && data){
            logger.info('Mongo DB DATA!!!', JSON.stringify(data));
            var dbStatement         = data.statement;
            var display             = [];
            var date;
            var amount;
            var ref;
            var desc;
            var currency;
            var message;

            if(dbStatement.length == 1){
                date            = dateFormat(dbStatement[0].TrDate, "yy-mm-dd");
                currency        = dbStatement[0].Currency;
                amount          = dbStatement[0].TrAmount;
                ref             = dbStatement[0].shortRef;
                desc            = dbStatement[0].TrDescription;
                message         = date + ' ' + currency + ' ' + amount + " Desc: " + desc + " Ref: " +  ref + "\n";
                display.push(message);

                callback(display, null);
            }else if(dbStatement.length > 1 && dbStatement.length < 6){
                for(var x = 0; x < dbStatement.length; x++){
                    date            = dateFormat(dbStatement[x].TrDate, "yy-mm-dd");
                    currency        = dbStatement[x].Currency;
                    amount          = dbStatement[x].TrAmount;
                    ref             = dbStatement[x].shortRef;
                    desc            = dbStatement[x].TrDescription;
                    message         = date + ' ' + currency + ' ' + amount + " Desc: " + desc + " Ref: " +  ref + "\n";
                    display.push(message);
                }
                callback(display, null);
            }else if(dbStatement.length > 5){
                for(var x = 0; x < 5; x++){
                    date            = dateFormat(dbStatement[x].TrDate, "yy-mm-dd");
                    currency        = dbStatement[x].Currency;
                    amount          = dbStatement[x].TrAmount;
                    ref             = dbStatement[x].shortRef;
                    desc            = dbStatement[x].TrDescription;
                    message         = date + ' ' + currency + ' ' + amount + " Desc: " + desc + " Ref: " +  ref + "\n";
                    display.push(message);
                }
                callback(display, null);
            }
        }else{
            callback(err, null);
        }
    });
};