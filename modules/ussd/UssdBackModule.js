exports.processRequest = function (sessionInformation, callback) {
    if (sessionInformation.input == '9') {
        sessionInformation.status = 'SUCCESS';
        sessionInformation.responseCode = 'UBM-00';
        sessionInformation.responseString = 'User returning to main menu';
        callback(sessionInformation);
    } else {
        sessionInformation.status = 'FAILURE';
        sessionInformation.responseCode = 'UBM-01';
        sessionInformation.responseString = 'User ending session';
        callback(sessionInformation);
    }
}