/**
 * Created by ntokozo on 2016/09/08.
 */
var esbAccess   = require('./connector/EsbAccessModule');
var log4js = require('log4js');
var logger = log4js.getLogger('PROCESS BILL');
var processBill = function(){};
processBill.prototype.getBill = function(sessionInfo, callback){
    sessionInfo.fromAcc = sessionInfo.accountNumber;
    esbAccess.transaction(sessionInfo, function(esbData){
        logger.info('__________________________BILL PROCESSED DATA: ', JSON.stringify(esbData));
        var products = esbData.data.products;
        var counter = 0;
        if(esbData){
            var prodName = [];
            var BILLSKU  = [];
            var BILLID   = [];
            for (var x = 0; x < products.length; x++){
                counter = counter + 1;
                prodName.push(counter + ' . ' + products[x].name + '\n');
                BILLSKU.push(products[x].sku);
                BILLID.push(products[x].id);
            }
            sessionInfo.display = prodName.join(',').replace(/,/g, '').split();
            sessionInfo.everestRef = esbData.data.everest_ref;
            sessionInfo.serviceBillSku = BILLSKU;
            sessionInfo.serviceBillID  = BILLID;
            callback(sessionInfo);
        }
    });
};

module.exports= new processBill();