var esbConnecter        = require('./connector/EsbAccessModule');
var log4js              = require('log4js');
var logger              = log4js.getLogger('AIRTIME PURCHASE MODULE');

var PNF                = require('google-libphonenumber').PhoneNumberFormat;
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

exports.processRequest=function(sessionInfo, callback){
    sessionInfo.fromAcc     = sessionInfo.accountNumber;
    msisdnFilter(sessionInfo.topupmsisdn,function (phoneNumber) {
        var isValid = phoneUtil.isValidNumber(phoneNumber, PNF.INTERNATIONAL);
        if(isValid === 'true'){
            sessionInfo.topupmsisdn = phoneUtil.format(phoneNumber, PNF.INTERNATIONAL);
        }
    });
    esbConnecter.transaction(sessionInfo, function (data) {
        var counter = 0;
        if(data){
            var providers=[];
            var MNOID=[];
            for(var x = 0; x < data.data.providers.length; x++){
                counter      = counter + 1;
                providers.push(counter + '. '+data.data.providers[x].name + '\n');
                MNOID.push(data.data.providers[x].id);
            }
            var newProviders = providers.join(',').replace(/,/g, '').split();
            sessionInfo.s_provider   = newProviders;
            sessionInfo.MNOID        = MNOID;
            sessionInfo.everestRef   = data.data.everest_ref;
            sessionInfo.responseCode = 'AIR-00';
            callback(sessionInfo);
        }
    })

    function msisdnFilter(toMsisdn, callback){
        var phoneNumber = phoneUtil.parse(toMsisdn, 'ZA');
        callback(phoneNumber);
    }
};
