var modConfig   = require('./moduleHandler/ModuleConfig');

exports.processRequest = function (sessionInformation, callback) {
    var message = modConfig.module.airtime.message;
    var bckMsg  = modConfig.module.bckMessage;
    var myself  = modConfig.module.airtime.options.myself;
    var other = modConfig.module.airtime.options.someone;
    sessionInformation.responseCode = 'POM-00';
    sessionInformation.responseString = 'Select Purchase Option: ';
    sessionInformation.response = [message + ' 1. ' + myself + '\n' + ' 2. ' + other + '\n' + bckMsg];
    callback(sessionInformation);
};