var modConfig           = require('./moduleHandler/ModuleConfig');

exports.processRequest  = function(sessionInfo, callback){
    sessionInfo.toAcc  = sessionInfo.input;
    var message=modConfig.module.transactionInter.amntMessage;
    sessionInfo.responseCode = 'PINTA-00';
    sessionInfo.response=[message];
    callback(sessionInfo);
};