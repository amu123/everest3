var airtimePurchase     = require('./AirtimePurchaseModule');
var transInter          = require('./TransactionInter');
var transMobile         = require('./TransactionMobile');
var modConfig           = require('./moduleHandler/ModuleConfig');
var log4js              = require('log4js');
var logger              = log4js.getLogger('PRE TRANSACTION');

exports.processRequest = function (sessionInformation, callback) {
    var option  = sessionInformation.input;
    var accounts;
    var message;
    sessionInformation.Option = sessionInformation.input;
    switch(option){
        case '1':
            logger.info('TRANSFER BETWEEN MY ACCOUNTS');
            sessionInformation.status = 'SUCCESS';
            accounts = JSON.parse(sessionInformation.accounts);
            message  = modConfig.module.transaction.message;
            if (accounts.length == 1) {
                var bckMessage = modConfig.module.oneAccMsg + '. ' + modConfig.module.bckMessage;
                sessionInformation.accountNumber = accounts[0].accountNumber;
                sessionInformation.responseCode = 'PTMS-11';
                sessionInformation.response = [bckMessage];
                callback(sessionInformation);
            }else {
                sessionInformation.findModule = modConfig.module.transaction.name;
                sessionInformation.modCode = modConfig.module.transaction.code;
                sessionInformation.responseCode = 'PTMS-00';
                sessionInformation.responseString = 'Select account';
                sessionInformation.response = [message + sessionInformation.accountDisplay];
                callback(sessionInformation);
            }
            break;
        case '2':
            logger.info('TRANSFER TO ANOTHER ACCOUNT');
            sessionInformation.status = 'SUCCESS';
            accounts = JSON.parse(sessionInformation.accounts);
            message  = modConfig.module.transaction.message;
            if (accounts.length == 1) {
                sessionInformation.accountNumber = accounts[0].accountNumber;
                sessionInformation.trans_type = accounts[0].product.tran_types[4].code;
                transInter.processRequest(sessionInformation, function(toAccData){
                    sessionInformation.responseCode = toAccData.responseCode;
                    sessionInformation.response = [toAccData.display];
                    callback(sessionInformation);
                });
            }else {
                sessionInformation.findModule = modConfig.module.transactionInter.name;
                sessionInformation.modCode = modConfig.module.transactionInter.code;
                sessionInformation.responseCode = 'PTMS-00';
                sessionInformation.responseString = 'Select account';
                sessionInformation.response = [message + sessionInformation.accountDisplay];
                callback(sessionInformation);
            }
            break;
        case '3':
            logger.info('TRANSFER TO MOBILE');
            sessionInformation.status = 'SUCCESS';
            accounts = JSON.parse(sessionInformation.accounts);
            message  = modConfig.module.transaction.message;
            if (accounts.length == 1) {
                sessionInformation.accountNumber = accounts[0].accountNumber;
                sessionInformation.trans_type = accounts[0].product.tran_types[4].code;
                transMobile.processRequest(sessionInformation, function(mobileData){
                    sessionInformation.responseCode = mobileData.responseCode;
                    sessionInformation.response = [mobileData.display];
                    callback(sessionInformation);
                });
            }else {
                sessionInformation.findModule = modConfig.module.transactionMobile.name;
                sessionInformation.modCode = modConfig.module.transactionMobile.code;
                sessionInformation.responseCode = 'PTMS-00';
                sessionInformation.responseString = 'Select account';
                sessionInformation.response = [message + sessionInformation.accountDisplay];
                callback(sessionInformation);
            }
            break;
        case '4':
            logger.info('LOAN PAYMENT');
            sessionInformation.status = 'SUCCESS';
            accounts = JSON.parse(sessionInformation.accounts);
            message  = modConfig.module.transaction.message;
            if (accounts.length == 1) {
                sessionInformation.accountNumber = accounts[0].accountNumber;
                sessionInformation.trans_type = accounts[0].product.tran_types[6].code;
                transInter.processRequest(sessionInformation, function (transLoanData) {
                    sessionInformation.responseCode = transLoanData.responseCode;
                    sessionInformation.response = [transLoanData.display];
                    callback(sessionInformation);
                })
            }else {
                sessionInformation.findModule = modConfig.module.transactionLoan.name;
                sessionInformation.modCode = modConfig.module.transactionLoan.code;
                sessionInformation.responseCode = 'PTMS-00';
                sessionInformation.responseString = 'Select account';
                sessionInformation.response = [message + sessionInformation.accountDisplay];
                callback(sessionInformation);
            }
            break;
        case '9':
            logger.info('LOAN PAYMENT');
            sessionInformation.status = 'SUCCESS';
            sessionInformation.responseCode ='BPTMS-00';
            sessionInformation.responseString = 'User returning to main menu';
            callback(sessionInformation);
    }
};