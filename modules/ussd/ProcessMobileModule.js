var esbAccess          = require('./connector/EsbAccessModule');
var log4js             = require('log4js');
var modConfig          = require('./moduleHandler/ModuleConfig');
var logger             = log4js.getLogger('PROCESS MOBILE TRANSFER:');

var PNF                = require('google-libphonenumber').PhoneNumberFormat;
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

exports.processRequest = function(sessionInfo, callback){
    msisdnFilter(sessionInfo.msisdn /*sessionInfo.topupmsisdn*/,function (phoneNumber) {
        logger.info('_____________MSISDN____________', phoneUtil.format(phoneNumber, PNF.INTERNATIONAL));
        sessionInfo.value = phoneUtil.format(phoneNumber, PNF.INTERNATIONAL);
    });

    var request = {
        transType   :sessionInfo.trans_type,
        fromAcc     :sessionInfo.fromAcc,
        toMsisdn    :sessionInfo.value,
        amount      :sessionInfo.input,
        search      :modConfig.module.transactionMobile.search,
        reference   :'test',
        MSISDN      :sessionInfo.value
    };
    logger.info('_____________THE_REQUEST____________', request);

    sessionInfo.searchData = modConfig.module.transactionMobile.search;
    sessionInfo.amount = sessionInfo.input;
    esbAccess.transaction(sessionInfo, function(esbResponse){
        if(esbResponse){
            logger.info('_____________THE_RESPONSE____________', esbResponse);
            sessionInfo.responseCode = 'ESBR-00';
            callback(sessionInfo);
        }
    });

    function msisdnFilter(toMsisdn, callback){
        var phoneNumber = phoneUtil.parse(toMsisdn, 'ZA');
        callback(phoneNumber);
    }
};