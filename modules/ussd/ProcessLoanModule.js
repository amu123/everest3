var esbAccess   = require('./connector/EsbAccessModule');
var log4js      = require('log4js');
var logger      = log4js.getLogger('PROCESS LOAN MODULE: ');
exports.processRequest = function(sessionInfo, callback){
    sessionInfo.ref     = 'test';
    sessionInfo.amount  = sessionInfo.fullAmnt + '.' + sessionInfo.input;
    esbAccess.transaction(sessionInfo, function(esbData){
        if(esbData){
            var _code = esbData.data.code;
            var message ='';
            switch(_code){
                case '00':
                    message = esbData.data.message + '\n' + esbData.data.everest_ref  + '\n' +  ' Press 9 to return to Main Menu';
                    sessionInfo.responseCode = 'MIB-00';
                    sessionInfo.response = [message];
                    break;
                case '08':
                    message = esbData.data.message;
                    sessionInfo.responseCode = 'MIB-01';
                    sessionInfo.response = [message];
                    break;
            }
        }else{
            sessionInfo.responseCode = 'MIB-03';
            sessionInfo.response = ['Exit, Press 9 to return to Main Menu'];
        }
        callback(sessionInfo);
    });
};
