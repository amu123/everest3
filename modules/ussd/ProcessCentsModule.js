var modConfig   = require('./moduleHandler/ModuleConfig');
var procCode    = require('./ProcessTransType');
exports.processRequest=function(sessionInfo, callback){
    var option = sessionInfo.trans_type;

    procCode.getCode(option, function(resp){
        sessionInfo.fullAmnt =sessionInfo.input;
        var message = modConfig.module.centsMsg;
        sessionInfo.responseCode = resp;
        sessionInfo.response = [message];
        callback(sessionInfo);
    });
};