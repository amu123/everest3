var modConfig       = require('./moduleHandler/ModuleConfig');
var log4js          = require('log4js');
var logger          = log4js.getLogger('TRANS MOBILE HANDLER');

 exports.processRequest  = function(sessionInfo, callback){
  logger.info('________________________________________THE_INPUT', sessionInfo.input);
 sessionInfo.msisdn  = sessionInfo.input;
 var message=modConfig.module.transactionInter.amntMessage;
 sessionInfo.responseCode = 'PMOBI-00';
 sessionInfo.response=[message];
 callback(sessionInfo);
 };
