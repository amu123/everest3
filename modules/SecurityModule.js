/**
 * Created by ntokozo on 2016/07/08.
 */
var rsaKeyText = '-----BEGIN RSA PRIVATE KEY-----\n' +
    'MIICWQIBAAKBgQClPNX8SRyMBMNNSwL3T5qzAwsD21tuenh/py1jUfRlEwrZoSK4\n' +
    'hmTTrt3yll+d4bnAixxKixMYmZA7cNrKTHggdJGPUX5rRqvU68NSDc8jq/Z5pc8f\n' +
    '4Y/VDQcw4/7eytVF4BvtUYsXdhC0A2SmyVNrhh8eTjcEKWM1mzsH8F+FzwIBEQKB\n' +
    'gDCZbB0Gcc7UOXEWD+5itQd5XZe4/MYkBVK4sv8YGrRQ5RLVCjZFpS8zbnSGlJfY\n' +
    '+mXOj9mwbwc8OXrk9Q5Suer0lody6xbHKptx0eVG7U3vpKw7FK9W/BCXJ1uztJ0+\n' +
    'qiTdbnE2x7/Hq1jOQafbNcRtRlJY43X8SEEwPTyNKGqRAkEAz/+3LV+zfVZwW7B/\n' +
    '2UxA9UH8oFTSKTfrUmZZRMx2E028vUilTA8OyQeJy/VAsXw6AnkHdISbt1276bBn\n' +
    'GQb8PwJBAMte2GoynYqRVA0V+dz1pofrADyaX/b+5IWBbKKZHuJ4Pv7CLX5ufJ/C\n' +
    'leCQK2G5/BAdZUUTcOUPz0C6bJECUnECQD0tF8INFqxkt6J/NKlSqbGL4OPctkhb\n' +
    'va7SzvYeBJxEClXKEn/IMYZroP/Amo6N8vGrIE9yS+qjGSadS3/U4McCQQC/aFM2\n' +
    'xjnr1BLfI739I2+O+0uEVQ8GlYvI8kggkB0RYh0sAgyzOs+la9ha4gq2VLEAG6qb\n' +
    'XZduLP9L+sCIeqfxAj91gA6E3yiwJhGCyV5fZtdbjnhug3NNJ54v587l7lczvSb1\n' +
    'gyjj3TDv+4+lma0T0E9Uw3HlZiDqeE31nJU1fak=\n' +
    '-----END RSA PRIVATE KEY-----';

var log4js = require('log4js'),
    NodeRSA = require('node-rsa');

var logger = log4js.getLogger('RSA SECURITY');
logger.setLevel('INFO');

// var key = new NodeRSA(fs.readFileSync('./config/everest.pem', 'utf-8'), { encryptionScheme: {
//  scheme: 'pkcs1'
//  } });

var rsaKey = new NodeRSA(rsaKeyText, {
    encryptionScheme: {
        scheme: 'pkcs1'
    }
});

exports.processRequest = function (sessionInformation, callback) {
    //console.log('ENCRYPTED TEXT!!: ', sessionInformation);
    callback(rsaKey.decrypt(sessionInformation.encryptedText).toString());
};

exports.encryptData = function (sessionInformation, callback) {
    var encryptedText = rsaKey.encrypt(sessionInformation.clearText).toString('base64');
    logger.debug(encryptedText);
    callback(encryptedText);
};
