var async = require('async'),
    UssdSession = require('../modules/ussd/UssdSessionModule'),
    express = require('express'),
    router = express.Router();

var logger = require('log4js').getLogger('WEB SERVER ROUTES');
logger.setLevel('DEBUG');
/**=========AUTHENTICATION*/

var sessions = {};

router.post('/wizzit_everest_ussd', function(req, res) {
    logger.info('ROUTER RECEIVE');
    var responseJson = {};

    try{
        var json = req.body;
        logger.info('JSON: ', json);

        new UssdSession().processSession(json, function(ussdInfo){
            logger.debug('ROUTER RESPOND');
            ussdInfo.returnString=ussdInfo.display;
            if(ussdInfo.continueSession){
                ussdInfo.type=1;
            }else{
                ussdInfo.type=3;
            }
            var responseObject={
                type:ussdInfo.type,
                display:ussdInfo.display,
                msisdn:ussdInfo.msisdn
            }
            logger.info('RESPONSEOBJECT!: ', responseObject);
            res.send(JSON.stringify(responseObject));
        })
    }catch (e){
        logger.error(e.stack);
        responseJson.display = e.message;
        responseJson.type = 3;
        response.send(JSON.stringify(responseJson));
    }
});

module.exports = router;