/**
 * Created by takonewa on 2016/01/26.
 */
//var parser          =   require('xml-parser');
var mapper          =   require('./map_helper');
var async           =   require('async');
var xml2js          =   require('xml2js');
var builder         =   new xml2js.Builder();
var options         =   {explicitArray: false, tagNameProcessors:[xml2js.processors.stripPrefix]};


function ISOMParser(){

}
ISOMParser.prototype.buildJSON  =   function(xml, callback){
    async.parallel({isom: function(cb){
        xml2js.parseString(xml, options, function(err, json) {
            if(!err && json){
                var user        =   json.message.authHeader['$'];
                var data        =   json.message.isomsg.field;
                var response    =   {};
                async.forEachOf(data, function iterator(value, key, next){
                    var element =   value['$'];
                    response[element.id]  = element.value;

                    next();
                }, function _done(){
                    cb(null, {user: user, value:response});
                });
            }else{
                cb('#isom.parser.error');
            }
        });
    }}, function done(err, result){
        if(!err && result){
            var data    =   {};
            var isom    =   result.isom.value;
            var user    =   result.isom.user;
            async.forEachOf(mapper, function process(value, key,  next){
                data[value] = isom[key];
                next();
            }, function _done(){
                callback(null, data, user);
            });
        }else{
            callback(err);
        }
    });
}
ISOMParser.prototype.buildISOM  =   function(json, callback){
    var data    =   {
        message:{
            isomsg:{
                '$':{direction: "response"},
                field:[]
            }
        }
    };
    var inner   =   [];
    if(json.data)
    {
        async.forEachOf(mapper, function process(value, key,  next){
            if(json[value]){
                inner.push({'$':{id:key, value: json[value]}})
            }
            next();
        }, function done(){
            data.message.isomsg.field =   inner;
            var xml = builder.buildObject(data);
            async.forEachOf(mapper, function process(value, key,  next){
                if(json.data[value]){
                    inner.push({'$':{id:key, value: json.data[value]}})
                }
                next();
            }, function done(){
                data.message.isomsg.field =   inner;
                var xml = builder.buildObject(data);
                callback(null, xml);
            });
        });

    }else
    {
        async.forEachOf(mapper, function process(value, key,  next){
            if(json[value]){
                inner.push({'$':{id:key, value: json[value]}})
            }
            next();
        }, function done(){
            data.message.isomsg.field =   inner;
            var xml = builder.buildObject(data);
            callback(null, xml);
        });
    }
}
module.exports  =  new ISOMParser();