/*
 Database connection module
 */

var EventEmitter = require('events').EventEmitter,
    util = require('util'),
    mongoose = require('mongoose'),
    log4js = require('log4js');
    mongoose.set('debug', true);

var fs = require('fs');

var engine;
var logger;

function MongoDbEngine() {

    engine = this;

    var Configuration = require('../configuration/MongoDbConfiguration');

    logger = log4js.getLogger(Configuration.loggerName);
    logger.setLevel(Configuration.loggerLevel);

    var database;

    function connect() {

        var certFileBuf = fs.readFileSync(Configuration.certLoc);


        var options = {
            server: {
                poolSize: Configuration.poolSize,
                ssl: true,
                sslValidate: true,
                //sslAllowInvalidCertificates: true,
                sslCA: certFileBuf
                //sslKey: key,
                //sslCert:key
            },
            auth: {
                authdb: Configuration.authdb
            },
            user: Configuration.user,
            pass: Configuration.pass
        };

        mongoose.connect(Configuration.url,options);
        database = mongoose.connection;

        database.on('connecting', function () {
            logger.debug('CONNECTING', Configuration.url);
        });

        database.on('connected', function () {
            logger.debug('CONNECTED', Configuration.url);
        });

        database.on('open', function () {
            logger.info('OPEN');
            logger.info('READY');
            engine.emit('ready');
        });

        database.on('disconnecting', function () {
            logger.warn('DISCONNECTING');
        });

        database.on('disconnected', function () {
            logger.warn('DISCONNECTED');
        });

        database.on('close', function () {
			logger.warn('CLOSE');
			mongoose.disconnect();
			engine.emit('connect');
			
        });

        database.on('reconnected', function () {
            logger.debug('RECONNECTED', Configuration.url);
        });

        database.on('fullsetup', function () {
            logger.info('fullsetup');
        });

        database.on('error', function (error) {
            logger.error(error.stack);
            mongoose.disconnect();
        });
    }

    engine.once('start', connect);
    engine.on('connect', connect);

    EventEmitter.call(engine);
}

util.inherits(MongoDbEngine, EventEmitter);

module.exports = {
    getComponent: function () {
        if (!engine) {
            engine = new MongoDbEngine();
        }
        return engine;
    }
};

