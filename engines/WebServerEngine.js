"use strict";

var EventEmitter = require('events').EventEmitter,
    util = require('util');

var engine;
var logger;

function WebServerEngine() {

    engine = this;

    var bodyParser = require('body-parser'),
        http = require('http'),
        express = require('express'),
        path = require('path'),
        log4js = require('log4js');

    var EverestWebApp = express();
    var EverestWebServer;

    var Configuration = {
        port: 9097,
        host: '0.0.0.0',
        loggerName: 'WEB SERVER'
    };

    logger = log4js.getLogger(Configuration.loggerName);

    function start() {
        logger.debug('STARTING');

        //EverestExpressApp.use(express.favicon(''));

        /*### AUTO LEVEL DETECTION
         http responses 3xx, level = WARN
         http responses 4xx & 5xx, level = ERROR
         else.level = INFO
         Leaving comments in for now from copy/paste job
         */
        // TODO
        EverestWebApp.use(log4js.connectLogger(logger, {level: 'auto'}));

        // Handles escaped characters
        EverestWebApp.use(bodyParser.urlencoded({
            extended: true
        }));

        EverestWebApp.use(bodyParser.json({}));

        EverestWebApp.use('/', require('../routes/WebServerRoutes'));

        engine.emit('listen');
    }

    function listen() {
        EverestWebServer = http.createServer(EverestWebApp);
        EverestWebServer.listen(Configuration.port, Configuration.host, function () {
            logger.debug('WEB SERVER LISTENING ON: ' + Configuration.port);
            logger.info('READY');
            engine.emit('ready');
        });
    }

    engine.once('listen', listen);
    engine.once('start', start);

    EventEmitter.call(engine);
}

util.inherits(WebServerEngine, EventEmitter);

module.exports = {
    getComponent: function () {
        if (!engine) {
            engine = new WebServerEngine();
        }
        return engine;
    }
};